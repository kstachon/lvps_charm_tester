from Instruments.DMM.DAQ6510 import DAQ6510
from Instruments.Load.IT8700 import IT8700
from Instruments.PS.EA_PS_9500 import EA_PS_9500
from Instruments.PS.RS_HMC8042 import HMC8042
from CAN.uc_test import UCTest

from TestSuite.configuration_handler import ConfigurationHandler

from time import sleep, perf_counter
from datetime import datetime
import os
import logging
import threading

import numpy as np

from PyQt5 import QtCore

class TestBase(QtCore.QObject):
    pre_result_signal = QtCore.pyqtSignal(list)
    post_result_signal = QtCore.pyqtSignal(list)
    
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        self._test_name = ''
        self._test_time = None
        self._test_status = False # The status could be: 0: FAILED, 1: PASSED, -1: ABORTED
        self._test_result = ''
        self._test_duration = -1

        

    def get_signals(self):
        return self.pre_result_signal, self.post_result_signal

    def get_row(self):
        print(f'{[self._test_time, self._test_name, self._test_status, self._test_duration, self._test_result]}')

    def emit_pre_result(self):
        self.pre_result_signal.emit([self._test_time, self._test_name, 'In progress...'])

    def emit_post_result(self):
        self.post_result_signal.emit([self._test_status, self._test_duration, self._test_result])


class TestList(TestBase):
    def __init__(self):
        super().__init__()
        self.NO_VALUE = -1
        
        
        self.dmm_V_in = DAQ6510('192.168.100.101')
        self.dmm_V_out = DAQ6510('192.168.100.102')

        self.dmm_I_in = DAQ6510('192.168.100.106')

        self.eloads = [IT8700('192.168.100.110'), IT8700('192.168.100.111'), IT8700('192.168.100.112')]
        
        self.ps = EA_PS_9500('192.168.100.105')
        
        self.interlock = HMC8042('192.168.100.103')
        

        try:
            self.uc_test = UCTest()
            self.can_connected = True
            config = self.get_config()
            self.uc_test.can_uc.rxTimeout = config['CAN_rx_timeout']
            self.uc_test.can_uc.txTimeout = config['CAN_tx_timeout']
        except:
            logging.info('Initialization of CAN communication failed!')
            self.can_connected = False

        self.channels_to_test = [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 201, 202, 203, 204, 205, 206, 301, 302, 303]
        self.channels_to_test_CAN = [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 201, 202, 203, 204, 205, 206]

        self.monitoring_active = False

        self.ps.set_output(410, 7, 2500)
       

    def map_ch(self, ch):
        mapping = {
            'channels' : [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 201, 202, 203, 204, 205, 206, 301, 302, 303],
            'DMM_chan' : [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 211, 212, 213, 214, 215, 216, 217, 218, 219],
            'eload_chassis' : [0, 0, 0, 0, 0, 0, 0, 0, 1 ,1 ,1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1],
            'eload_chan' : [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 5, 6, 7],
            'chan_max_current' : [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 10, 20, 20, 20],
            'LVPS_module' : [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 1, 1, 1],
            'LVPS_chan' : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 4, 5, 7, 8, 2, 5, 8], 
            'LVPS_current_exponent': [-9, -9, -9 ,-9, -9, -9 ,-9, -9, -9 ,-9, -9, -9 ,-9, -9, -9 ,-9, -9, -9 ,-8, -8, -8]
        } 
        idx = mapping['channels'].index(ch)
        ret = {
            'DMM_chan' : mapping['DMM_chan'][idx],
            'eload_chassis' : mapping['eload_chassis'][idx],
            'eload_chan' : mapping['eload_chan'][idx],
            'chan_max_current' : mapping['chan_max_current'][idx],
            'LVPS_module' : mapping['LVPS_module'][idx],
            'LVPS_chan' : mapping['LVPS_chan'][idx],
            'LVPS_current_exponent' : mapping['LVPS_current_exponent'][idx]
        }
        return ret
    
    def get_config(self):
        return ConfigurationHandler('Configuration/config.yaml').read_dict()
    
    def test_custom(self):
        #This test can be implemented at any time during radiation test if needed
        start_time = perf_counter()
        self._test_time = datetime.today()
        self._test_name = 'CAN communication'
        self.emit_pre_result()
        
        config = self.get_config()
        self.uc_test.can_uc.rxTimeout = config['CAN_rx_timeout']
        self.uc_test.can_uc.txTimeout = config['CAN_tx_timeout']
        
        # TEST CAN COMMMUNICATION
        MODULES = [3, 5, 1]
        u_input = []

        for i, module in enumerate(MODULES):
                try:
                    status, temp1, temp2, uInput = self.uc_test.get_module_measure_0(module)
                except:
                    uInput = -1
                u_input.append(uInput)

        datapoint = {
            'time' : str(datetime.today()),

            'M1_u_input': u_input[0],
            'M2_u_input': u_input[1],
            'M3_u_input': u_input[2]
        }
        global_log_name = f'Results/test_CAN_communication/global_log.tsv'
        self.log_datapoint(global_log_name, datapoint)
        
        

        self._test_status = False # The status could be: 0: FAILED, 1: PASSED, -1: ABORTED
        if all(u > 350 for u in u_input):
            self._test_status = True
        self._test_result = f'U_input [Module 1, Module 2, Module 3]: [{u_input[0]:.1f}, {u_input[1]:.1f}, {u_input[2]:.1f}]'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()

    def test_custom2(self):
        #This test can be implemented at any time during radiation test if needed
        start_time = perf_counter()
        self._test_time = datetime.today()
        self._test_name = 'Custom 2'
        self.emit_pre_result()
        

        '''
        CH = 303
        LOAD = 20
        ch_info = self.map_ch(CH)

        
        self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], LOAD) # set current of a load
        self.eloads[ch_info['eload_chassis']].enable_ch_single(ch_info['eload_chan'])

        sleep(1)

        self._acquire_datapoint_once()

        self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], 0) # set current of a load

        self._acquire_datapoint_once()

        '''
        self.interlock.disable_interlock_power()


        '''
        sleep(10)

        self.interlock.enable_interlock_power()

        sleep(1)
        
        
        CH = [301,]

        for ch in CH:
            ch_info = self.map_ch(ch)
            self.uc_test.switch_channel_on_with_values(ch_info['LVPS_module'], ch_info['LVPS_chan'], 5, 10, current_exponent=ch_info['LVPS_current_exponent'])
        
        sleep(0.2)

        for ch in CH:
            ch_info = self.map_ch(ch)
            logging.info(f"READOUT: {self.eloads[ch_info['eload_chassis']].get_IV_all()}")
    '''
        
        

        self._test_status = False # The status could be: 0: FAILED, 1: PASSED, -1: ABORTED
        self._test_result = 'Example result'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()

    def test_digitize_single(self, CH, DURATION, RATE):
        #This test can be implemented at any time during radiation test if needed
        start_time = perf_counter()
        self._test_time = datetime.today()
        self._test_name = 'User digitize'
        self.emit_pre_result()
        
        self._acquire_datapoint_once()

        logging.info(f'Digitizing: CH: {CH}, DURATION: {DURATION}, RATE: {RATE}')
        
        DMM_CH = self.map_ch(CH)['DMM_chan']

        self.dmm_V_out.continuous_one_init_dig(DMM_CH, DURATION, RATE, label='user')
        self.dmm_I_in.front_continuous_one_init_dig_CURRENT(DURATION, RATE, label='user')
        self.dmm_V_in.front_continuous_one_init_dig(DURATION, 1000, label='user')
        self.dmm_V_out.continuous_trigger()
        self.dmm_I_in.continuous_trigger()
        self.dmm_V_in.continuous_trigger()

        chan_info = self.map_ch(CH)
        sleep(0.5)
        self.uc_test.set_channel_on_off(chan_info['LVPS_module'], chan_info['LVPS_chan'], False)
        sleep(1)
        self.uc_test.switch_channel_on_with_values(chan_info['LVPS_module'], chan_info['LVPS_chan'], 8, 11)
        
        sleep(DURATION)

        self.dmm_V_out.continuous_fetch_dig()
        self.dmm_I_in.continuous_fetch_dig()
        self.dmm_V_in.continuous_fetch_dig()

        self._acquire_datapoint_once()
        

        self._test_status = True # The status could be: 0: FAILED, 1: PASSED, -1: ABORTED
        self._test_result = 'See log files.'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()
    
    
    
    def test_reboot(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        file_name = f'Results/test_reboot/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'Reboot test'
        self.emit_pre_result()

        config = self.get_config()
        
        
        self.dmm_V_in.front_continuous_one_init(label='reboot')
        self.dmm_I_in.front_continuous_one_init_CURRENT(label='reboot')
        self.dmm_V_out.continuous_all_init(label='reboot')
        self.dmm_V_in.continuous_trigger_auto_fetch()
        self.dmm_I_in.continuous_trigger_auto_fetch()
        self.dmm_V_out.continuous_trigger_auto_fetch()
        
        sleep(1) # this is needed to have buffer filled

        self.interlock.disable_interlock_power()

        sleep(0.5)

        #Do stuff
        ret = []
        ret.append(self._acquire_datapoint_once(filename=file_name))
        print(self.ps.measure_all())
        self.ps.turn_off() 
        sleep(5)
        print(self.ps.measure_all())
        ret.append(self._acquire_datapoint_once(filename=file_name))
       
      
        sleep(config['test_reboot_time'])
        #ret.append(self._acquire_datapoint_once(filename=file_name))
        self.ps.turn_on()
        sleep(3)

        print(self.ps.measure_all())
        ret.append(self._acquire_datapoint_once(filename=file_name))

        
        self.dmm_V_in.continuous_abort()
        self.dmm_V_out.continuous_abort()
        self.dmm_I_in.continuous_abort()
        

        result = [f"{el['V_in']:.1f} V" for el in ret]

        self._test_status = False
        if ret[1]['V_in'] < 10 and ret[2]['V_in'] > 350:
            self._test_status = True

        self._test_result = f'[V_before, V_during, V_after]: {result}'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()


    def test_idle_current(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        file_name = f'Results/test_idle_current/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'Idle current'
        self.emit_pre_result()

        config = self.get_config()
        LABEL = 'idle_current'

        for eload in self.eloads:
            eload.disable_ch_all()

        self.dmm_I_in.front_continuous_one_init_CURRENT(label=LABEL)
        self.dmm_V_in.front_continuous_one_init(label=LABEL)
        self.dmm_I_in.continuous_trigger_auto_fetch()
        self.dmm_V_in.continuous_trigger_auto_fetch()

        sleep(0.5)
        
        self.interlock.disable_interlock_power()
        sleep(1)
        ret = (self._acquire_datapoint_once(filename=file_name))
        I_disable = ret['I_in']
        self.interlock.enable_interlock_power()
        sleep(0.5)

        ret = (self._acquire_datapoint_once(filename=file_name))
        I_off = ret['I_in']

        #Turn on all channels and set voltages to V
        set_V = config['test_idle_current_voltage']
        channels = config['active_channels']
        for chan in channels:
            chan_info = self.map_ch(chan)
            set_OCP = config['test_idle_current_OCP']*chan_info['chan_max_current']
            LVPS_module = chan_info['LVPS_module']
            LVPS_chan = chan_info['LVPS_chan']
            LVPS_curr_exponent = chan_info['LVPS_current_exponent']
            self.uc_test.switch_channel_on_with_values(LVPS_module, LVPS_chan, set_OCP, set_V, LVPS_curr_exponent)
            #sleep(0.05) #ADDED BECAUSE MODULE 2 DID NOT START CHANNELS(ONLY ONE STARTED)

        sleep(1)
        #Measure_all
        ret = (self._acquire_datapoint_once(filename=file_name))

        self.dmm_I_in.continuous_abort()
        self.dmm_V_in.continuous_abort()

        I_idle = ret['I_in']

        V_array = [f"{ret['V_out_'+str(item)]:.1f}" for item in channels]
        result_voltages = f'V_outs: {V_array}'
        result_str = f'I_disable = {I_disable:.3f}\nI_off = {I_off:.3f} A\nI_idle = {I_idle:.3f} A\n{result_voltages}'


        self._test_status = False
        if I_disable > 0.06 and I_disable < 0.08 and I_idle > 0.27 and I_idle < 0.35:
            self._test_status = True

        self._test_result = result_str
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()


    def test_on_off(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        file_name = f'Results/test_on_off/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'Turn on/off'
        self.emit_pre_result()
        config = self.get_config()

        set_OCP = config['test_enable_disable_OCP']
        set_V = config['test_enable_disable_voltage']

        set_eload_CC_relative = config['test_enable_disable_load']
        
        ch_to_test = config['active_channels']
        
        # Turn off all channels
        for chan in ch_to_test:
            chan_info = self.map_ch(chan)
            LVPS_module = chan_info['LVPS_module']
            LVPS_chan = chan_info['LVPS_chan']
            self.uc_test.set_channel_on_off(LVPS_module, LVPS_chan, False)

        #set eload to certain load
        for ch in ch_to_test:
            ch_info = self.map_ch(ch)
            set_CC = set_eload_CC_relative*ch_info['chan_max_current']
            self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], set_CC) # set current of a load

        volt_on = []
        volt_off = []
        for ch in ch_to_test:
            chan = self.map_ch(ch)

            label = f'on_off_CH_{ch}'
            self.dmm_V_out.continuous_one_init_dig(chan['DMM_chan'], 1, rate=10000, label=label)
            self.dmm_I_in.front_continuous_one_init_dig_CURRENT(1, 10000, label=label)
            self.dmm_V_in.front_continuous_one_init_dig(1, 1000, label=label)
            self.dmm_V_out.continuous_trigger()
            self.dmm_I_in.continuous_trigger()
            self.dmm_V_in.continuous_trigger()
            self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], set_OCP, set_V, current_exponent=chan['LVPS_current_exponent'])
            sleep(0.5)
            self.uc_test.set_channel_on_off(chan['LVPS_module'], chan['LVPS_chan'], False)

            sleep(0.2)
            ret = self.dmm_V_out.continuous_fetch_dig()
            self.dmm_I_in.continuous_fetch_dig()
            self.dmm_V_in.continuous_fetch_dig()
            
            volt_on.append(ret[4000][1])
            volt_off.append(ret[9000][1])
            #print([ret[4000][1], ret[9999][1]]) #beda zwrocone wartosci z polowy i konca

        channels_list = [ch for ch in ch_to_test]
        voltages_on_list = [f'{volt:.1f}' for volt in volt_on]
        voltages_off_list = [f'{volt:.1f}' for volt in volt_off]


        self._test_status = True
        if any(item < 5 for item in volt_on):
            self._test_status = False


        self._test_result = f'CH: {channels_list}\nV_on: {voltages_on_list}\nV_off: {voltages_off_list}'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()

    def test_voltage_scan(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        global_log_name = f'Results/test_voltage_scan/global_log.tsv'
        file_name = f'Results/test_voltage_scan/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'Voltage scan'
        self.emit_pre_result()
        
        config = self.get_config()
        VOLTAGES_TO_TEST = config['test_scan_voltages']
        SET_OCP = 8

        LABEL = 'voltage_scan'
        
        self.dmm_I_in.front_continuous_one_init_CURRENT(label=LABEL)
        self.dmm_V_in.front_continuous_one_init(label=LABEL)
        self.dmm_I_in.continuous_trigger_auto_fetch()
        self.dmm_V_in.continuous_trigger_auto_fetch()


        OVP_trip = [0]*len(config['active_channels'])
        
        self.interlock.enable_interlock_power()

        names = ['V_out_'+str(ch) for ch in config['active_channels']]
        for volt in VOLTAGES_TO_TEST:
            #set all voltages to
            for ch in config['active_channels']:
                chan = self.map_ch(ch)
                self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], SET_OCP, volt, current_exponent=chan['LVPS_current_exponent'])
            sleep(0.5)
            measurement = (self._acquire_datapoint_once(filename=file_name))
            voltages = [measurement[name] for name in names]
            if all(volt<5 for volt in voltages):
                break
            for i, voltage in enumerate(voltages):
                if voltage > OVP_trip[i]:
                    OVP_trip[i] = voltage

            sleep(4) #additional delay

        
        self.dmm_I_in.continuous_abort()
        self.dmm_V_in.continuous_abort()
        
        # create datapoint
        datapoint = {'time': str(self._test_time)}
        for i, ch in enumerate(self.channels_to_test):
            datapoint[str(ch)] = -1
        
        for i, ch in enumerate(config['active_channels']):
            
            datapoint[str(ch)] = OVP_trip[i]

        self.log_datapoint(global_log_name, datapoint)
        
        result_str = [f'{volt:.2f}' for volt in OVP_trip]

        self._test_status = True
        if (not all((config['test_scan_voltages_LB'] < el < config['test_scan_voltages_UB']) for el in OVP_trip)):
            self._test_status = False


        self._test_result = f'OVP: {result_str}'
        
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()


    def test_OCP_scan(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        global_log_name = f'Results/test_OCP_scan/global_log.tsv'
        file_name = f'Results/test_OCP_scan/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'OCP scan'
        self.emit_pre_result()

        config = self.get_config()

        SET_VOLTAGE = config['test_OCP_scan_voltage']
        
        self.interlock.enable_interlock_power()

        OCP_scan_settings = config['test_OCP_scan_settings']
        OCP_scan_currents = config['test_OCP_scan_currents']
        channels_to_test = config['active_channels']


        thresholds = []
        
        #Enable all loads
        for eload in self.eloads:
            eload.enable_ch_all()

        for OCP_setting in OCP_scan_settings:
            threshold = [0]*len(channels_to_test)
            ch_status = [True]*len(channels_to_test)

            for ch in channels_to_test:
                ch_info = self.map_ch(ch)
                self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], 0)

            # set_OCP of a DUT for each chan
            for ch in channels_to_test:
                chan = self.map_ch(ch)
                OCP_set = OCP_setting
                #print(f'OCP_set: {OCP_set}')
                self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], OCP_set, SET_VOLTAGE, current_exponent=chan['LVPS_current_exponent'])

            for curr in OCP_scan_currents:
                #Set all loads to a value
                for ch in channels_to_test:
                    ch_info = self.map_ch(ch)
                    current = curr*ch_info['chan_max_current']/100
                    self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], current)
                
                sleep(0.6)

                meas = self._acquire_datapoint_once(filename=file_name)
                names = ['V_out_'+str(ch) for ch in channels_to_test]
                voltages = [meas[name] for name in names]
                #print(str(voltages))
                

                for i, voltage in enumerate(voltages):
                    ch_info = self.map_ch(channels_to_test[i])
                    if (voltage >= 5) and (ch_status[i] == True):
                        threshold[i] = curr*ch_info['chan_max_current']/100
                    elif voltage < 5:
                        ch_status[i] = False
                #print(str(threshold))
                
                #Break if all channels already tripped
                if not any(ch_status):
                    # Disable all loads
                    for ch in channels_to_test:
                        ch_info = self.map_ch(ch)
                        self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], 0)
                    break
            
            thresholds.append(threshold)

            datapoint = {'time' : self._test_time, 
                        'setting' : OCP_setting }
            
            for chan in self.channels_to_test:
                datapoint[str(chan)] = -1
            for meas, chan in zip(threshold, channels_to_test):
                datapoint[str(chan)] = meas
            self.log_datapoint(global_log_name, datapoint)

        result_str = ''
        for threshold, OCP_setting in zip(thresholds, OCP_scan_settings):
            result_str+=f'OCP_setting: {OCP_setting}, \t OCP_measured: {threshold}\n'


        self._test_result = f'{result_str}'
        self._test_status = True
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()


    def test_interlock(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        file_name = f'Results/test_interlock/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        self._test_name = 'Interlock'
        self.emit_pre_result()

        config = self.get_config()
        SET_VOLTAGE = config['test_interlock_set_voltage']
        
        
        self.interlock.enable_interlock_power()
        sleep(1)
        # Enable all channels
        for ch in config['active_channels']:
            chan = self.map_ch(ch)
            OCP_set = config['test_interlock_set_OCP']*chan['chan_max_current']
            curr = config['test_interlock_load']*chan['chan_max_current']
            self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], OCP_set, SET_VOLTAGE, current_exponent=chan['LVPS_current_exponent'])
            self.eloads[chan['eload_chassis']].set_CC_single(chan['eload_chan'], curr)
        for eload in self.eloads:
            eload.enable_ch_all()
        sleep(1)
        meas_before = self._acquire_datapoint_once(filename=file_name)
        
        #monitor V_in, I_in
        self.dmm_V_in.front_continuous_one_init_dig(0.4, 50000, label='interlock')
        self.dmm_I_in.front_continuous_one_init_dig_CURRENT(0.4, 200000, label='interlock')
        self.dmm_V_in.continuous_trigger()
        self.dmm_I_in.continuous_trigger()
        sleep(0.1)

        self.interlock.disable_interlock_power()
        sleep(0.5)
        self.dmm_V_in.continuous_fetch_dig()
        self.dmm_I_in.continuous_fetch_dig()
        meas_after = self._acquire_datapoint_once(filename=file_name)
        
        
        
        names = ['V_out_'+str(ch) for ch in config['active_channels']]
        voltages_before = [f'{meas_before[name]:.1f}' for name in names]
        voltages_after = [f'{meas_after[name]:.1f}' for name in names]

        self._test_status = True
        if any(meas_after[name] > 1 for name in names):
            self._test_status = False

        self._test_result = f'V_before: {voltages_before}\nV_after: {voltages_after}'
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()

    


    def test_load(self):
        start_time = perf_counter()
        self._test_time = datetime.today()
        self._test_name = 'Load test'
        self.emit_pre_result()


        config = self.get_config()
        load_results = {'channel' : []}
        
        ## Prepare for test
        #Disable all channels
        self.interlock.disable_interlock_power()
        sleep(0.5)

        # activate interlock voltage
        self.interlock.enable_interlock_power()

        sleep(2)

        # Measure idle current
        I_in_idle = self.dmm_I_in.front_measure_single_CURRENT() # For the moment contant value
        logging.info(f'I_in_idle = {I_in_idle}')
        

        for ch in config['active_channels']:
            ch_info = self.map_ch(ch)
            for volt in config['load_test_voltages']:
                file_name = f'Results/test_load/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}_CH_{ch}_{volt:01}V.tsv'
                
                ## PREPARATION
                self.uc_test.switch_channel_on_with_values(ch_info['LVPS_module'], ch_info['LVPS_chan'], 1.2*ch_info['chan_max_current'], volt, current_exponent=ch_info['LVPS_current_exponent'])
                sleep(0.05)
                self.eloads[ch_info['eload_chassis']].enable_ch_single(ch_info['eload_chan'])
                sleep(0.5)
                
                for curr in config['load_test_currents']:
                    print(f'CH: {ch}, V: {volt}, I: {curr}')

                    #SET BEFORE MEASUREMENT
                    current = curr*ch_info['chan_max_current']/100
                    self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], current) # set current of a load
                    
                    sleep(0.6) #Time needed for stabilization
                    
                    # MEASURE
                    lab = f'test_load_{volt:01}V_{curr}'
                    self.dmm_V_in.front_continuous_one_init_dig(0.2, label=lab, rate=20000)
                    
                    self.dmm_V_out.continuous_one_init_dig(ch_info['DMM_chan'], 0.02, label=lab, rate=200000)
                    
                    self.dmm_I_in.front_continuous_one_init_dig_CURRENT(0.02, label=lab, rate=200000)

                    self.dmm_V_in.continuous_trigger()
                    self.dmm_V_out.continuous_trigger()
                    self.dmm_I_in.continuous_trigger()

                    I_in_ps = 0
                    #I_in_ps = self.ps.measure_all()['A']
                    I_out = self.eloads[ch_info['eload_chassis']].get_current_single(ch_info['eload_chan'])

                    #logging.info(f'I_out = {I_out}')
                    

                    sleep(0.1)
                    

                    V_in_list = self.dmm_V_in.continuous_fetch_dig()
                    V_out_list = self.dmm_V_out.continuous_fetch_dig()
                    I_in_list = self.dmm_I_in.continuous_fetch_dig()
                    

                    V_in_raw =  np.array(V_in_list)[:,1]
                    V_in_mean = np.mean(V_in_raw)
                    V_in_sd = np.std(V_in_raw)
                    V_in_ptp = np.ptp(V_in_raw)

                    V_out_raw =  np.array(V_out_list)[:,1]
                    V_out_mean = np.mean(V_out_raw)
                    V_out_sd = np.std(V_out_raw)
                    V_out_ptp = np.ptp(V_out_raw)

                    I_in_raw =  np.array(I_in_list)[:,1]
                    I_in_mean = np.mean(I_in_raw)
                    I_in_sd = np.std(I_in_raw)
                    I_in_ptp = np.ptp(I_in_raw)

                    
                    datapoint = {
                        'V_in' : V_in_mean,
                        'I_in' : I_in_mean,
                        'V_out' : V_out_mean,
                        'I_out' : I_out,
                        'V_in_sd' : V_in_sd,
                        'V_in_ptp' : V_in_ptp,
                        'V_out_sd' : V_out_sd,
                        'V_out_ptp' : V_out_ptp,
                        'I_in_sd' : I_in_sd,
                        'I_in_ptp' : I_in_ptp,
                        'I_in_ps' : I_in_ps,
                        'I_out_set' : curr,
                        'V_out_set' : volt,
                        'I_idle' : I_in_idle,
                        'efficiency' : (V_out_mean*I_out)/(V_in_mean*(I_in_mean-I_in_idle))
                                 }
                    self.log_datapoint(file_name, datapoint)

                # set current to 0 before next measurement
                self.eloads[ch_info['eload_chassis']].set_CC_single(ch_info['eload_chan'], 0)

                #Turn off channel which was just tested
                self.uc_test.set_channel_on_off(ch_info['LVPS_module'], ch_info['LVPS_chan'], False)

                #TODO add fast scann of interlock
                '''self.dmm_V_out.continuous_one_init_dig(ch_info['DMM_chan'], 0.02, label=lab, rate=200000)
                self.dmm_V_out.continuous_trigger()'''

        self._test_result = f'See files for details...'
        self._test_status = True
        self._test_duration = perf_counter()-start_time
        self.emit_post_result()

    

    def test_monitor_all_start(self):
        self.start_time = perf_counter()
        self._test_time = datetime.today()
        
        #file_name = f'Results/test_monitor_all/{self._test_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}.tsv'
        file_name = f'Results/test_monitor_all/CHARM_MONITORING.tsv'
        self._test_name = 'Monitoring'
        self.emit_pre_result()

        config = self.get_config()
        SET_VOLTAGE = config['monitor_all_voltage']
        LOAD = config['monitor_all_load']
        ACTIVE_CHANNELS = config['active_channels']
        LOGGING_INTERVAL = config['monitor_all_logging_interval']
        
        # Set interlock voltage
        self.interlock.enable_interlock_power()
        sleep(0.5)


        for ch in config['active_channels']:
            chan = self.map_ch(ch)
            OCP_set = chan['chan_max_current']*1.2
            self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], OCP_set, SET_VOLTAGE, current_exponent=chan['LVPS_current_exponent'])

        
        #Set e-loads
        for ch in ACTIVE_CHANNELS:
            eload_chassis = self.map_ch(ch)['eload_chassis']
            eload_chan = self.map_ch(ch)['eload_chan']
            max_current = self.map_ch(ch)['chan_max_current']
            set_current = max_current*LOAD
            self.eloads[eload_chassis].set_CC_single(eload_chan, set_current)
            self.eloads[eload_chassis].enable_ch_single(eload_chan)


        
        self.dmm_V_in.front_continuous_one_init(label='mon')
        self.dmm_V_out.continuous_all_init(label='mon')
        self.dmm_I_in.front_continuous_one_init_CURRENT(label='mon')
        self.dmm_V_in.continuous_trigger_auto_fetch()
        self.dmm_V_out.continuous_trigger_auto_fetch()
        self.dmm_I_in.continuous_trigger_auto_fetch()
        
        sleep(1)

        self.start_logging_deamon(LOGGING_INTERVAL, filename=file_name)
        self.monitoring_active = True

    
    def test_monitor_all_stop(self):
        if self.monitoring_active:
            # THIS MUST BE FIRST
            self.stop_logging_deamon()

            # Turn off eloads
            for eload in self.eloads:
                eload.disable_ch_all()
            

            self.dmm_V_in.continuous_abort()
            self.dmm_V_out.continuous_abort()
            self.dmm_I_in.continuous_abort()
            sleep(0.1)
            #These seems to be not needed #TODO below
            self.dmm_V_in.continuous_fetch()
            self.dmm_V_out.continuous_fetch()
            self.dmm_I_in.continuous_fetch()

            self._test_result = f'Monitoring finished'
            self._test_status = True
            self._test_duration = perf_counter()-self.start_time
            self.emit_post_result()
            self.monitoring_active = False

    def _turn_on_all_ch(self, SET_VOLTAGE):
        config = self.get_config()
        for ch in config['active_channels']:
            chan = self.map_ch(ch)
            OCP_set = chan['chan_max_current']
            for i in range(4):
                self.uc_test.switch_channel_on_with_values(chan['LVPS_module'], chan['LVPS_chan'], OCP_set, SET_VOLTAGE, current_exponent=chan['LVPS_current_exponent'])
                #if successful break


    def log_datapoint(self, filename, data):
        if not os.path.exists(filename):
            with open(filename, 'w') as file:
                # Create the header row with column names
                header = '\t'.join(data.keys())
                file.write(header + '\n')
        with open(filename, 'a') as file:
            # Convert the dictionary values to a tab-separated string and write it to the file
            values = '\t'.join(str(data[key]) for key in data.keys())
            file.write(values + '\n')        
    
    def _acquire_datapoint_once(self, filename=''):
        if filename == '':
            filename = f'Results/test_monitor_all/general_log.tsv'

        MODULES = [3, 5, 1]
        DMM_CHAN_TO_TEST = [self.map_ch(item)['DMM_chan'] for item in self.channels_to_test]

        I_in_ps = 0
        #I_in_ps = self.ps.measure_all()['A'] #todo

        if not self.dmm_V_in.background_measurement:
            V_in = self.dmm_V_in.front_measure_single()
        else:
            V_in = self.dmm_V_in.continuous_fetch_get_latest_reading()

        if not self.dmm_I_in.background_measurement:
            I_in = self.dmm_I_in.front_measure_single_CURRENT()
        else:
            I_in = self.dmm_I_in.continuous_fetch_get_latest_reading()

        # get V_outs
        if not self.dmm_V_out.background_measurement:
            V_out = self.dmm_V_out.measure_single_all()
        else:
            V_out = self.dmm_V_out.continuous_fetch_get_latest_reading_all()
        
        V_out_dict = {}
        for ch in self.channels_to_test:
            key = 'V_out_' + str(ch)
            try:
                dmm_ch = self.map_ch(ch)['DMM_chan']
                V_out_dict[key] = V_out[dmm_ch]       
            except ValueError:
                V_out_dict[key] = None

        
        # Get I_out
        I_out_raw = []
        for eload in self.eloads:
            I_out_raw.append(eload.get_IV_all()['currents'])
        
        I_out_dict = {}
        for ch in self.channels_to_test:
            key = 'I_out_' + str(ch)
            try:
                mapped = self.map_ch(ch)
                value = I_out_raw[mapped['eload_chassis']][mapped['eload_chan']-1]
                I_out_dict[key] = value
            except ValueError:
                I_out_dict[key] = None

        

        VI_interlock = self.interlock.get_actual_VI()
        V_interlock, I_interlock = VI_interlock['voltage'], VI_interlock['current']

        MODULES_can_error_count = [0]*len(MODULES)
        #CAN values
        
        readouts = {}
        for i, module in enumerate(MODULES):
            try:
                status, temp1, temp2, uInput = self.uc_test.get_module_measure_0(module)
                u_Aux = self.uc_test.get_module_measure_1(module)
                idle_loop_count, error_count = self.uc_test.get_counter_idle_ram(module)

                MODULES_can_error_count[i] = 0


                readout = {f'M{i+1}_status' : status,
                        f'M{i+1}_temp1' : temp1,
                        f'M{i+1}_temp2' : temp2,
                        f'M{i+1}_V_in' : uInput,
                        f'M{i+1}_u_aux' : u_Aux,
                        f'M{i+1}_idle_loop_count' : idle_loop_count,
                        f'M{i+1}_error_count' : error_count,
                }
            
            except ValueError:
                MODULES_can_error_count[i] +=1
                logging.info(f'No can communication in M{i+1}. Count: {MODULES_can_error_count[i]}')
                readout = {f'M{i+1}_status' : self.NO_VALUE,
                    f'M{i+1}_temp1' : self.NO_VALUE,
                    f'M{i+1}_temp2' : self.NO_VALUE,
                    f'M{i+1}_V_in' : self.NO_VALUE,
                    f'M{i+1}_u_aux' : self.NO_VALUE,
                    f'M{i+1}_idle_loop_count' : self.NO_VALUE,
                    f'M{i+1}_error_count' : self.NO_VALUE,
                }
            readouts.update(readout)


        for ch in self.channels_to_test:
            ch_info = self.map_ch(ch)
            if MODULES_can_error_count[MODULES.index(ch_info['LVPS_module'])] == 0:
                try:
                    status, V, I = self.uc_test.get_channel_measure_0(ch_info['LVPS_module'], ch_info['LVPS_chan'], current_exponent=ch_info['LVPS_current_exponent'])
                except ValueError:
                    status, V, I = -2, -2, -2
                try:
                    u_set_readback, i_set_readback = self.uc_test.get_channel_measure_1(ch_info['LVPS_module'], ch_info['LVPS_chan'])
                except ValueError:
                    u_set_readback, i_set_readback = -2, -2
            else:
                status, V, I, u_set_readback, i_set_readback = self.NO_VALUE, self.NO_VALUE, self.NO_VALUE, self.NO_VALUE, self.NO_VALUE

            readout = {
                f'status_{ch}' : status, 
                f'V_sense_{ch}' : V, 
                f'I_sense_{ch}' : I, 
                f'V_readback_{ch}' : u_set_readback, 
                f'I_readback_{ch}' : i_set_readback
            }
            readouts.update(readout)
        
        '''
        logging.info('No CAN communication!')

        readouts = {}
        for i, module in enumerate(MODULES):
            readout = {f'M{i+1}_status' : self.NO_VALUE,
                    f'M{i+1}_temp1' : self.NO_VALUE,
                    f'M{i+1}_temp2' : self.NO_VALUE,
                    f'M{i+1}_V_in' : self.NO_VALUE,
                    f'M{i+1}_u_aux' : self.NO_VALUE,
                    f'M{i+1}_idle_loop_count' : self.NO_VALUE,
                    f'M{i+1}_error_count' : self.NO_VALUE,
            }
            readouts.update(readout)
        for ch in self.channels_to_test:
            readout = {
                f'status_{ch}' : self.NO_VALUE, 
                f'V_sense_{ch}' : self.NO_VALUE, 
                f'I_sense_{ch}' : self.NO_VALUE, 
                f'V_readback_{ch}' : self.NO_VALUE, 
                f'I_readback_{ch}' : self.NO_VALUE
            }
            readouts.update(readout)
            }
            '''

        
        
        datapoint = {
                'time' : str(datetime.today()),
                'V_in' : V_in,
                'I_in' : I_in,
                'I_in_ps' : I_in_ps,
                'V_interlock' : V_interlock,
                'I_interlock' : I_interlock
            }
        datapoint.update(I_out_dict)
        datapoint.update(V_out_dict)
        datapoint.update(readouts)

        self.log_datapoint(filename, datapoint)
        return datapoint
    

    def digitize_single_ch(self, ch, duration, rate):
        print(str([ch, duration, rate]))

    def run_sequence(self):
        '''
        self.test_custom()
        self.test_custom2()
        '''

        config = self.get_config()
        include_reboot = config['include_reboot']
        
        self.test_monitor_all_stop()

        self.test_custom()
        self.test_idle_current()
        self.test_on_off()
        self.test_voltage_scan()
        self.test_OCP_scan()
        self.test_interlock()
        self.test_load()
        if include_reboot:
            self.test_reboot()

        self.test_monitor_all_start()

        
        

    # Logging deamon below
    def start_logging_deamon(self, interval, filename=''):
        self.stop_event = threading.Event()
        self.next_iteration_event = threading.Event()
        #self.instr_mutex = threading.Lock()
        self.thread = threading.Thread(target=self.repeat_function, args=(self._acquire_datapoint_once, interval, filename, self.stop_event, self.next_iteration_event))
        self.thread.daemon = True  # Set the thread as a daemon so it exits when the main program exits
        self.thread.start()

    def stop_logging_deamon(self):
        self.stop_event.set()
        self.thread.join()
    
    def repeat_function(self, func, interval, filename, stop_event, next_iteration_event):
        while not stop_event.is_set():
            # Wait for either 1 second or the next iteration event
            result = self.next_iteration_event.wait(timeout=interval)

            #if stop_event.is_set():
            #    break
            func(filename=filename)
            next_iteration_event.clear()  # Clear the event flag


        

if __name__ == "__main__":
    test_suite = TestList()
    test_suite.test_load()
    
    
