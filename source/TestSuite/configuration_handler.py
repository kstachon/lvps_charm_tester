import yaml

class ConfigurationHandler:
    """
    This class is responsible for creating a dictionary
    from the test procedure declaration file and writing
    the result dictionary to a file.
    """

    def __init__(self, filename):
        self.filename = filename
    
    def read_dict(self):
        """
        Reads yaml file and returns corresponding dict,
        change this method if you want to use another
        input filetype.
        """
        data = None
        with open(self.filename, 'r') as infile:
            data = yaml.safe_load(infile)
        return data

    def write_dict(self, data):
        """
        Write dictionary to yaml file with the given
        filename, change this method if you want to use
        another output filetype (e.g. CSV or root files)
        """
        with open(self.filename, 'w') as outfile:
            yaml.dump(data, outfile)



if __name__=="__main__":
    
    test_data = {
        'test_name' : 'beta',
        'channels' : [0, 1],
        'currents' : [0.1, 0.5, 1],
        'voltages' : [10,],
        'channels_iterator' : -1,
        'currents_iterator' : -1,
        'voltages_iterator' : -1
    }
    
    a = ConfigurationHandler('conf_test.yaml')
    #a.write_dict(test_data)
    ret1 = a.read_dict()
    print(ret1['voltages'][0])

    ret = ConfigurationHandler('fast_test.yaml').read_dict()

    print(ret)
    ConfigurationHandler('fast_test_written.yaml').write_dict(ret)