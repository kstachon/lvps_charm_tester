import sys
import os
import pandas as pd
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QWidget,
    QListWidget,
    QPushButton,
    QFileDialog,
)
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar,
)
from matplotlib.figure import Figure
import numpy as np

class WaveformViewer(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Waveform Viewer')
        self.setGeometry(100, 100, 800, 400)

        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        layout = QVBoxLayout()
        central_widget.setLayout(layout)

        # Listbox for displaying waveform files (fixed size)
        self.file_listbox = QListWidget()
        self.file_listbox.setFixedWidth(200)
        layout.addWidget(self.file_listbox)

        # Matplotlib plot (scalable with window size)
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        layout.addWidget(self.canvas)

        # Navigation toolbar for zoom and pan
        self.nav_toolbar = NavigationToolbar(self.canvas, self)
        layout.addWidget(self.nav_toolbar)

        # Create a container widget to hold the layout
        container_widget = QWidget()
        container_widget.setLayout(layout)
        central_layout = QVBoxLayout()
        central_layout.addWidget(container_widget)

        central_widget.setLayout(central_layout)

        # "Browse" button
        browse_button = QPushButton('Browse')
        browse_button.clicked.connect(self.select_data_directory)
        central_layout.addWidget(browse_button)

        # Set the initial directory
        self.data_directory = 'path_to_your_waveform_files'

        # Populate the listbox with files from the directory
        self.populate_file_listbox()

        # Create and plot example data
        self.plot_example_data()

    def populate_file_listbox(self):
        # Clear the listbox
        self.file_listbox.clear()

        # Get a list of waveform files in the directory
        if os.path.exists(self.data_directory) and os.path.isdir(self.data_directory):
            waveform_files = [file for file in os.listdir(self.data_directory) if file.endswith('.txt')]

            # Add files to the listbox
            self.file_listbox.addItems(waveform_files)

    def select_data_directory(self):
        # Open a file dialog to select the data directory
        directory = QFileDialog.getExistingDirectory(self, 'Select Data Directory', self.data_directory)
        if directory:
            self.data_directory = directory
            self.populate_file_listbox()

    def load_waveform(self, item):
        # Load and plot the selected waveform file using pandas
        file_path = os.path.join(self.data_directory, item.text())

        try:
            df = pd.read_csv(file_path, sep='\t', header=None)

            # Create subplots for each pair of columns
            num_plots = df.shape[1] // 2
            self.figure.clear()

            for i in range(num_plots):
                ax = self.figure.add_subplot(num_plots, 1, i + 1)
                time = df[i * 2]
                voltage = df[i * 2 + 1]

                ax.plot(time, voltage)
                ax.set_title(f'Waveform {i + 1}')
                ax.set_xlabel('Time')
                ax.set_ylabel('Voltage')

            # Refresh the canvas to display the new plots
            self.canvas.draw()

        except Exception as e:
            print(f"Error loading waveform: {e}")

    def plot_example_data(self):
        # Example data for the initial plot
        t = np.linspace(0, 1)
        y1 = 2 * np.sin(2 * np.pi * t)
        y2 = 4 * np.sin(2 * np.pi * 2 * t)

        # Create a subplot for the example data
        ax = self.figure.add_subplot(1, 1, 1)
        line1, = ax.plot(t, y1, lw=2, label='1 Hz')
        line2, = ax.plot(t, y2, lw=2, label='2 Hz')

        # Add legend and make lines toggleable
        leg = ax.legend(fancybox=True, shadow=True)
        lines = [line1, line2]
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            # On the pick event, find the original line corresponding to the legend
            # proxy line, and toggle its visibility.
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            # Change the alpha on the line in the legend, so we can see what lines
            # have been toggled.
            legline.set_alpha(1.0 if visible else 0.2)
            self.figure.canvas.draw()

        # Connect the pick event to the toggle function
        self.figure.canvas.mpl_connect('pick_event', on_pick)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = WaveformViewer()
    window.show()
    sys.exit(app.exec_())
