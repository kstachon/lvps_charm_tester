from PyQt5 import QtGui, QtWidgets
import logging
from PyQt5.QtWidgets import QTableWidgetItem, QTableWidget

from time import strftime, sleep
from datetime import datetime

class TestResultsTable():
    _column_count = 5
    _headers = ['Date', 'Test name', 'Status', 'Duration', 'Result']
    _tests = []

    def __init__(self, tableWidget_object: QtWidgets.QTableWidget):
        self.tableWidget_object = tableWidget_object
        
        self.tableWidget_object.setColumnCount(self._column_count)
        self.tableWidget_object.setHorizontalHeaderLabels(self._headers)
        
        
      
        self.tableWidget_object.setColumnWidth(0, 140)
        self.tableWidget_object.setColumnWidth(1, 80)
        self.tableWidget_object.setColumnWidth(2, 80)
        self.tableWidget_object.setColumnWidth(3, 60)
       




        #self.tableWidget_object.setColumnWidth(0, int(30 * (tableWidget_object.width()) / 100))
        #self.tableWidget_object.setColumnWidth(1, int(30 * (tableWidget_object.width()) / 100))
        #self.tableWidget_object.setColumnWidth(2, int(10 * (tableWidget_object.width()) / 100))
        #self.tableWidget_object.setColumnWidth(3, int(20 * (tableWidget_object.width()) / 100))
        

    def add_row(self, items):
        row_position = self.tableWidget_object.rowCount()
        self.tableWidget_object.insertRow(row_position)
        for idx, el in enumerate(items):
            self.tableWidget_object.setItem(row_position, idx, QTableWidgetItem(el))

        return row_position

    def add_result_pre(self, time:datetime, test_type):
        time_str = time.strftime("%Y-%m-%d %H.%M.%S")
        test_type_str = test_type
        status_str = 'In progress...'
        items = [time_str, test_type_str, status_str, '', '']
        row_number = self.add_row(items)

        color = QtGui.QColor(255, 255, 204)
        self.paint_row(row_number, color)

        # Scroll to the bottom to ensure the new row is visible
        self.tableWidget_object.scrollToBottom()

    def add_result_post(self, status, duration, result):
        color_passed = QtGui.QColor(200,255,200)
        color_failed = QtGui.QColor(255,200,200)

        row_position = self.tableWidget_object.rowCount()-1
        if status:
            status_str = 'Passed'
        else:
            status_str = 'Failed'

        duration_str = f'{duration:.1f} s'
        result_str = result

        items = [status_str, duration_str, result_str]

        for col in range(2,5):
            self.tableWidget_object.setItem(row_position,col,QtWidgets.QTableWidgetItem(items[col-2]))
            #Add tooltip
            if col==4:
                self.tableWidget_object.item(row_position, col).setToolTip(items[col-2])

        if status:
            self.paint_row(row_position, color_passed)
        else:
            self.paint_row(row_position, color_failed)
    
    def paint_row(self, row, color):
        for col in range(self._column_count):
            self.tableWidget_object.item(row, col).setBackground(color)

    def modify_row(self, items):
        for col in range(2,5):
            self.tableWidget_object.setItem(row,col,QtWidgets.QTableWidgetItem(items[col-1]))

    
    def modify_full_row(self, row, items):
        #last_row = self.tableWidget_object.rowCount()
        #self.tableWidget_object.insertRow(last_row)
        for col in range(0, 5):
            self.tableWidget_object.setItem(row, col, QtWidgets.QTableWidgetItem(items[col]))
        self.set_row_color(row, items[2])


    def clear_results(self):
        self.tableWidget_object.setRowCount(0)
        #self.tableWidget_object.setRowCount(self._row_count)
        self.set_test_list(self._tests)
    
    def set_row_color(self, row, result):
        color_passed = QtGui.QColor(200,255,200)
        color_failed = QtGui.QColor(255,200,200)
        if result == 'Passed':
            color = color_passed
        else:
            color = color_failed
        for col in range(0, 5):
          self.tableWidget_object.item(row, col).setBackground(color)




if __name__ == "__main__":

    from PyQt5 import QtWidgets
    import sys
    app = QtWidgets.QApplication(sys.argv)
    
    sys.exit(app.exec_())