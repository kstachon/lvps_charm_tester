import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import pandas as pd
import numpy as np
    
x = [1, 2, 3]
y = [2,3,2]

ch0, = plt.plot(x, y,'r', label='CH0', linewidth=0.7,visible=True)    
ch1, = plt.plot(x, y, label='CH1', linewidth=0.7,visible=True)   
ch2, = plt.plot(x, y, label='CH2', linewidth=0.7,visible=True)    
ch3, = plt.plot(x, y, label='CH3', linewidth=0.7,visible=True)    
ch4, = plt.plot(x, y, label='CH4', linewidth=0.7,visible=True)
ch5, = plt.plot(x, y, label='CH5t', linewidth=0.7,visible=True)

channel = [ch0, ch1, ch2, ch3, ch4, ch5]
# plt.legend(loc='upper right', frameon=False)
plt.title('Piezosensor Output (Channel 0-5)', loc='center', pad=16 )
plt.xlabel('Time (s)')
plt.ylabel('Amplitude (V)')
plt.subplots_adjust(left=0.1, bottom=0.1, right=0.95, top=0.95)
label = ['CH0','CH1','CH2','CH3','CH4','CH5gg']
label_on = [True, True, True, True, True, True]
button_space = plt.axes([0.92, 0.4, 0.15, 0.15])
button = CheckButtons(button_space, label, label_on)

def set_visible(labels):
    i = label.index(labels)
    channel[i].set_visible(not channel[i].get_visible())
    plt.draw()
    
[rec.set_facecolor(channel[i].get_color()) for i, rec in enumerate(button.rectangles)]

button.on_clicked(set_visible)
plt.show()   