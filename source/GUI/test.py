import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget
from PyQt5.QtGui import QFont, QColor
from PyQt5.QtCore import Qt

class TestStandApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("Test Stand Application")

        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout(self.central_widget)

        self.test_table = QTableWidget(self)
        self.layout.addWidget(self.test_table)

        self.test_table.setColumnCount(4)
        self.test_table.setHorizontalHeaderLabels(["Test Name", "Trigger Time", "Status", "Validation"])

        self.run_all_tests_button = QPushButton("Run All Tests", self)
        self.run_all_tests_button.clicked.connect(self.run_all_tests)
        self.layout.addWidget(self.run_all_tests_button)

        self.test_buttons = []

        for i in range(8):
            test_button = QPushButton(f"Test {i + 1}", self)
            test_button.clicked.connect(self.run_individual_test)
            self.layout.addWidget(test_button)
            self.test_buttons.append(test_button)

        # Style the buttons to make them more modern-looking
        self.style_buttons()

        self.test_table.horizontalHeader().setStretchLastSection(True)
        self.test_table.setSelectionBehavior(QTableWidget.SelectRows)

    def style_buttons(self):
        button_style = """
            QPushButton {
                background-color: #3498db;
                color: white;
                border: none;
                border-radius: 5px;
                padding: 10px;
            }
            QPushButton:hover {
                background-color: #2980b9;
            }
        """
        for button in self.test_buttons:
            button.setStyleSheet(button_style)

        self.run_all_tests_button.setStyleSheet(button_style)

    def run_all_tests(self):
        # Simulate running all tests
        for i in range(8):
            self.add_test_result(f"Test {i + 1}", "Now", "Completed", "Pass")

    def run_individual_test(self):
        sender = self.sender()
        test_number = self.test_buttons.index(sender) + 1
        self.add_test_result(f"Test {test_number}", "Now", "Completed", "Pass")

    def add_test_result(self, test_name, trigger_time, status, validation):
        row_position = self.test_table.rowCount()
        self.test_table.insertRow(row_position)
        self.test_table.setItem(row_position, 0, QTableWidgetItem(test_name))
        self.test_table.setItem(row_position, 1, QTableWidgetItem(trigger_time))
        self.test_table.setItem(row_position, 2, QTableWidgetItem(status))
        self.test_table.setItem(row_position, 3, QTableWidgetItem(validation))

        # Highlight rows with status "Completed" in light green
        if status == "Completed":
            for col in range(4):
                self.test_table.item(row_position, col).setBackground(QColor(173, 255, 47))

def main():
    app = QApplication(sys.argv)
    window = TestStandApp()
    window.setGeometry(100, 100, 800, 400)
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
