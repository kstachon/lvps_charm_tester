from GUI.main_window_gen import Ui_MainWindow
from GUI.TestResultsTable import TestResultsTable
from TestSuite.TestSuite import TestList

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSignal, QObject

import logging

from datetime import datetime
import schedule


from time import sleep, strftime

import threading

class SignalManager(QObject):
    next_trigger_signal = pyqtSignal(str)

class AppWindow(QMainWindow, Ui_MainWindow, ):
    
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.load_stylesheet()
        self.show()

        #Custom connections
        self.pushButton_idle_current.clicked.connect(self.pushButton_idle_current_clicked)

        self.pushButton_custom_test_1.clicked.connect(self.pushButton_test_custom_1_clicked)

        self.pushButton_custom_test_2.clicked.connect(self.pushButton_test_custom_2_clicked)

        self.pushButton_enable_disable.clicked.connect(self.pushButton_enable_disable_clicked)

        self.pushButton_voltage_scan.clicked.connect(self.pushButton_test_voltage_scan_clicked)

        self.pushButton_interlock.clicked.connect(self.pushButton_interlock_clicked)

        self.pushButton_OCP_scan.clicked.connect(self.pushButton_OCP_scan_clicked)

        self.pushButton_load_scan.clicked.connect(self.pushButton_load_scan_clicked)

        self.pushButton_start_monitoring.clicked.connect(self.pushButton_start_monitoring_clicked)

        self.pushButton_stop_monitoring.clicked.connect(self.pushButton_stop_monitoring_clicked)

        self.pushButton_3.clicked.connect(self.pushButton_get_datapoint_clicked)

        self.pushButton_reboot.clicked.connect(self.pushButton_reboot_clicked)

        self.pushButton_test_all.clicked.connect(self.run_sequence)

        self.pushButton_digitize_single.clicked.connect(self.pushButton_digitize_single_clicked)

        self.checkBox_auto_trigger.stateChanged.connect(self.checkBox_auto_trigger_changed)

        self.pushButton_trigger_sequence.clicked.connect(self.update_scheduler)

        self.pushButton_digitize_single.clicked.connect(self.pushButton_digitize_single_clicked)

        # Objects creation
        self.results_table = TestResultsTable(self.tableWidget)
        self.tests = TestList()
        self.thread1 = threading.Thread()
        self.thread2 = threading.Thread()

        self.scheduler_check_interval = 1
        self.scheduler_hours = 2
        
        
        signal_pre_result, signal_post_result = self.tests.get_signals()
        self.signal_manager = SignalManager()
        self.signal_manager.next_trigger_signal.connect(self.update_label)

        
        #Connections to signals created by test
        signal_pre_result.connect(self.add_pre_result)
        signal_post_result.connect(self.update_post_result)

        self.run_schedule_subthread()

    
    def update_label(self, next_trigger_time):
        # Update the label text with the next trigger time
        self.label_6.setText(f"{next_trigger_time}")
        
    def add_pre_result(self, items):
        self.results_table.add_result_pre(items[0], items[1])
        #print(items)

    def update_post_result(self, items):
        self.results_table.add_result_post(items[0], items[1], items[2])

        


    def load_stylesheet(self):
        sshFile='GUI/main_stylesheet.ssh' # had to add "source/" here, otherwise main.py crashes
        #sshFile='Resources/dark-orange.shh'
        with open(sshFile,"r") as fh:
            self.setStyleSheet(fh.read())

    def pushButton_reboot_clicked(self):
        self.run_in_subthread(self.tests.test_reboot)
    
    def pushButton_idle_current_clicked(self):
        self.run_in_subthread(self.tests.test_idle_current)

    def pushButton_enable_disable_clicked(self):
        self.run_in_subthread(self.tests.test_on_off)

    def pushButton_test_voltage_scan_clicked(self):
        self.run_in_subthread(self.tests.test_voltage_scan)

    def pushButton_OCP_scan_clicked(self):
        self.run_in_subthread(self.tests.test_OCP_scan)

    def pushButton_interlock_clicked(self):
        self.run_in_subthread(self.tests.test_interlock)

    def pushButton_load_scan_clicked(self):
        self.run_in_subthread(self.tests.test_load)

    def pushButton_start_monitoring_clicked(self):
        self.run_in_subthread(self.tests.test_monitor_all_start)

    def pushButton_stop_monitoring_clicked(self):
        self.run_in_subthread(self.tests.test_monitor_all_stop)

    def pushButton_get_datapoint_clicked(self):
        self.run_in_subthread(self.tests._acquire_datapoint_once)

    


    def pushButton_test_custom_1_clicked(self):
        self.run_in_subthread(self.tests.test_custom)

    def pushButton_test_custom_2_clicked(self):
        self.run_in_subthread(self.tests.test_custom2)

    def run_sequence(self):
        self.run_in_subthread(self.tests.run_sequence)

    def pushButton_digitize_single_clicked(self):
        self.run_in_subthread(self.tests.test_digitize_single, self.spinBox_chan.value(), self.doubleSpinBox_duration.value(), self.doubleSpinBox_rate.value())

    
    #Schedule
    def run_schedule(self, trig_event):
        self.scheduler_enabled = True
        schedule.every(self.scheduler_hours).hours.at(":00").do(self.scheduled_function)
        while True:
            if self.scheduler_enabled:
                schedule.run_pending()
                
            next_job = schedule.next_run()
            next_job_str = next_job.strftime("%Y-%m-%d\n%H:%M:%S")
            try:
                self.signal_manager.next_trigger_signal.emit(next_job_str)
            except:
                pass
            self.trigger_event.wait(timeout=self.scheduler_check_interval)
            if self.trigger_event.is_set():
                self.trigger_event.clear()
                sleep(0.1)
                schedule.clear()
                schedule.every(self.scheduler_hours).hours.at(":00").do(self.scheduled_function)
                
                
        

    
    def run_schedule_subthread(self):
        if self.thread2.is_alive():
            self.thread2.join()
        self.trigger_event = threading.Event()
        self.thread2 = threading.Thread(target=self.run_schedule, args=(self.trigger_event,))
        self.thread2.daemon = True  # Set the thread as a daemon so it exits when the main program exits
        self.thread2.start()

    def checkBox_auto_trigger_changed(self, state):
        if state == 2:
            self.scheduler_enabled = True
        else:
            self.scheduler_enabled = False
        
    def scheduled_function(self):
        #next_job = schedule.default_scheduler.next_run
        #next_job_str = next_job.strftime("%Y-%m-%d\n%H:%M:%S")
        #self.signal_manager.next_trigger_signal.emit(next_job_str)
        self.run_sequence()

    def update_scheduler(self):
        self.scheduler_hours = self.spinBox_trigger_period.value()
        self.trigger_event.set()
        
    
    #schedule 2
    def scheduler(self, delay):
        while self.scheduler_enabled:
            pass

        # nice example: https://stackoverflow.com/questions/73322769/python-schedule-run-function-every-hour-on-the-hour



    


    
    def run_in_subthread(self, fun, *args):
        if self.thread1.is_alive():
            logging.info('I cannot run another measurement because there is one in progress!')
        else:
            self.thread1 = threading.Thread(target=fun, args=args)
            self.thread1.daemon = True  # Set the thread as a daemon so it exits when the main program exits
            self.thread1.start()