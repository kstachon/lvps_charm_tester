import logging
import pyvisa
import pyvisa_py
from datetime import datetime
from time import sleep
import numpy as np

from pathlib import Path
import os

import threading

class DAQ6510():
    def __init__(self, IP):
        #rm = pyvisa.ResourceManager() #alternative way
        rm = pyvisa.ResourceManager('@py')
        self.conn = rm.open_resource(f'TCPIP::{IP}::inst0::INSTR')

        self.reset_device()
        self.test_con()
        
        #Variables
        self.continuous_buffer_position = 0
        self.continuous_trigger_time = None
        self.continuous_data = None
        self.continuous_data_all = {}
        self.background_measurement = False
        self.continuous_one_selected_channel = -1

        self.V_in_SCALAR = 43.95194394

        #User params
        self.AUTO_FETCH_INTERVAL = 3
        self.VOTAGE_OUT_RANGE = 100
        self.VOLTAGE_OUT_RANGE_DIG = 100

        self.VOLTAGE_IN_RANGE = 100

    def test_con(self):
        ret = self.conn.query('*IDN?')
        logging.info(f'Connected to: {ret}')

    def reset_device(self):
        self.conn.write('*RST')

    def front_measure_single_CURRENT(self):
        start_timestamp = datetime.today()
        #self.conn.write(f':SEN:DIG:FUNC "VOLTage"; :SENS:DIG:COUN {digitizer_samples}; :SENS:DIGitize:VOLTage:SRAT {digitizer_rate}')
        self.conn.write(f'SENS:FUNC "CURR"')
        self.conn.write(f'SENS:CURR:NPLC 1')
        self.conn.write(f':SENS:CURR:RANG 10')
        ret = float(self.conn.query(f'MEAS:CURR?'))

        return ret
    
    def front_continuous_one_init_CURRENT(self, label=''):
        self.continuous_one_selected_channel = 1
        self.continuous_acquisition_type = 'single'
        self.label = label

        self._cont_init_common_prepare_buffer()

        self.conn.write(f'TRIG:LOAD "SimpleLoop", 1')
        self.conn.write(f':TRIGger:BLOCk:MDIGitize 3,  "defbuffer1", INF')
        self.conn.write(f':TRIGger:BLOCk:WAIT 4, COMMand')
        self.conn.write(f':TRIG:BLOC:NOP 5')

        self.conn.write(f':SENS:FUNC "CURR"')
        self.conn.write(f':SENS:CURR:RANG 10')
        self.conn.write(f':SENS:CURR:NPLC 0.2') # TODO was 0.5

        sleep(0.1)
        
        

    def front_continuous_one_init_dig_CURRENT(self, duration, rate=1000000, label=''):
        self.continuous_one_selected_channel = 1
        self.continuous_acquisition_type = 'dig'
        self.label = label
        
        self._cont_init_common_prepare_buffer()
        
        self.continuous_one_dig_points = rate*duration
        if self.continuous_one_dig_points > self.continuous_buffer_size:
            raise ValueError('Duration of continuous acquisition must not exceed buffer size!')

        self.conn.write(f'TRIG:LOAD "SimpleLoop", 1')
        self.conn.write(f':TRIGger:BLOCk:MDIGitize 3,  "defbuffer1", AUTO')
        self.conn.write(f':TRIG:BLOC:NOP 4')

        self.conn.write(f':SENS:DIG:FUNC "CURR"')
        self.conn.write(f':SENS:DIG:COUN {self.continuous_one_dig_points}')
        self.conn.write(f':SENS:DIGitize:CURR:SRAT {rate}')
        self.conn.write(f':SENS:DIG:CURR:RANG 10')
        


    def front_measure_single(self):
        start_timestamp = datetime.today()
        #self.conn.write(f':SEN:DIG:FUNC "VOLTage"; :SENS:DIG:COUN {digitizer_samples}; :SENS:DIGitize:VOLTage:SRAT {digitizer_rate}')
        self.conn.write(f'SENS:FUNC "VOLT"')
        self.conn.write(f'SENS:VOLT:NPLC 1')
        ret = float(self.conn.query(f'MEAS:VOLT?'))
        ret_scaled = ret*self.V_in_SCALAR
        
        ans = {'timestamp' : start_timestamp, 
               'reading' : ret_scaled}
        
        return ret_scaled
    
    
    def _cont_init_common_prepare_buffer(self):
        self.continuous_buffer_position = 0
        self.continuous_buffer_size = 6900000 #This should be MAX

        self.conn.write(f'TRAC:CLE')
        self.conn.write(f':TRACe:POINts {self.continuous_buffer_size}') 
    
    
    # This function is not used now...
    def _cont_init_common_digitizer(self, duration, rate):
        self._cont_init_common_prepare_buffer()
        self.continuous_one_selected_channel = 0
        self.continuous_acquisition_type = 'dig'

        self.continuous_one_dig_points = rate*duration
        if self.continuous_one_dig_points > self.continuous_buffer_size:
            raise ValueError('Duration of continuous acquisition must not exceed buffer size!')
        
        

    def front_continuous_one_init_dig(self, duration, rate=1000000, label=''):
        self.continuous_one_selected_channel = 0
        self.continuous_acquisition_type = 'dig'
        self.label = label
        
        self._cont_init_common_prepare_buffer()
        
        self.continuous_one_dig_points = rate*duration
        if self.continuous_one_dig_points > self.continuous_buffer_size:
            raise ValueError('Duration of continuous acquisition must not exceed buffer size!')



        self.conn.write(f'TRIG:LOAD "SimpleLoop", 1')
        self.conn.write(f':TRIGger:BLOCk:MDIGitize 3,  "defbuffer1", AUTO')
        self.conn.write(f':TRIG:BLOC:NOP 4')
        
        #print(self.conn.query(f':TRIGger:BLOCk:LIST?'))

        self.conn.write(f':SENS:DIG:FUNC "VOLTage"')
        self.conn.write(f':SENS:DIG:COUN {self.continuous_one_dig_points}')
        self.conn.write(f':SENS:DIGitize:VOLTage:SRAT {rate}')
        self.conn.write(f':SENS:DIG:VOLT:RANG {self.VOLTAGE_IN_RANGE}')
        #self.conn.write(':DIG:VOLT:INP MOHM10') # This changes input impedance to 10 MOhm instead of 10 GOhm (charge injection problem?)

        ''' math does not work for digitized signal
        self.conn.write(':CALC:VOLT:MATH:FORM MXB')
        self.conn.write(':CALC:VOLT:MATH:MMF 43.95194394')
        self.conn.write(':CALC:VOLT:MATH:STAT ON')
        '''
        
    def front_continuous_one_init(self, label=''):
        self.continuous_one_selected_channel = 0
        self.continuous_acquisition_type = 'single'
        self.label = label

        self._cont_init_common_prepare_buffer()

        self.conn.write(f':SENS:FUNC "VOLT"')
        self.conn.write(f':SENS:VOLT:NPLC 0.01') # TODO
        self.conn.write(f':SENS:VOLT:RANG {self.VOLTAGE_IN_RANGE}') 

        self.conn.write(f'TRIG:LOAD "SimpleLoop", 1')
        self.conn.write(f':TRIGger:BLOCk:MDIGitize 3,  "defbuffer1", INF')
        self.conn.write(f':TRIGger:BLOCk:WAIT 4, COMMand')
        self.conn.write(f':TRIG:BLOC:NOP 5')


        #print(self.conn.query(f':TRIGger:BLOCk:LIST?'))

        

    def measure_single_all(self, label=''):
        self.continuous_data_all = {}
        self.continuous_acquisition_type = 'all'
        self.label = label

        self._cont_init_common_prepare_buffer()
        
        scan_channels = '(@101:112, 211:219)'
        self.continuous_all_num_of_chan = 21
        
        self.conn.write(f'SENS:FUNC "VOLT", {scan_channels}')
        self.conn.write(f'SENS:VOLT:NPLC 1, {scan_channels}')
        self.conn.write(f':SENS:VOLT:RANG {self.VOTAGE_OUT_RANGE}, {scan_channels}')

        self.conn.write(f'ROUT:SCAN:CRE {scan_channels}')
        self.conn.write(f'ROUte:SCAN:COUN:SCAN 1')
        self.conn.write(f':ROUTe:SCAN:INTerval 0')

        self.continuous_trigger_time = datetime.today()
        self.conn.write(':INIT; *WAI')

        self.continuous_fetch()

        print(self.continuous_data_all)
        return self.continuous_data_all



    # Readout of all 21 inputs with a maximum rate
    def continuous_all_init(self, label=''):
        self.continuous_data_all = {}
        self.continuous_acquisition_type = 'all'
        self.label = label

        self._cont_init_common_prepare_buffer()

        scan_channels = '(@101:112, 211:219)'
        self.continuous_all_num_of_chan = 21
        
        
        self.conn.write(f'SENS:FUNC "VOLT", {scan_channels}')
        self.conn.write(f'SENS:VOLT:NPLC 1, {scan_channels}')
        self.conn.write(f':SENS:VOLT:RANG {self.VOTAGE_OUT_RANGE}, {scan_channels}')

        self.conn.write(f'ROUT:SCAN:CRE {scan_channels}')
        self.conn.write(f'ROUte:SCAN:COUN:SCAN 0')
        self.conn.write(f':ROUTe:SCAN:INTerval 0')

    def continuous_one_init(self, ch, label=''):
        self.continuous_one_selected_channel = self.match_chan(ch)
        self.continuous_acquisition_type = 'single'
        self.label = label

        self._cont_init_common_prepare_buffer()

        scan_channels = f'(@{self.continuous_one_selected_channel})'

        self.conn.write(f'SENS:FUNC "VOLT", {scan_channels}')
        self.conn.write(f'SENS:VOLT:NPLC 0.01, {scan_channels}')
        self.conn.write(f':SENS:VOLT:RANG {self.VOTAGE_OUT_RANGE}, {scan_channels}')

        self.conn.write(f'ROUT:SCAN:CRE {scan_channels}')
        self.conn.write(f'ROUte:SCAN:COUN:SCAN 0')
        self.conn.write(f':ROUTe:SCAN:INTerval 0')
    
    def continuous_one_init_dig(self, ch, duration, rate=1000000, label=''):
        self.continuous_one_selected_channel = self.match_chan(ch)
        self.continuous_acquisition_type = 'dig'
        self.label = label

        self._cont_init_common_prepare_buffer()
        
        scan_channels = f'(@{self.continuous_one_selected_channel})'
       
        
        self.continuous_one_dig_points = rate*duration
        if self.continuous_one_dig_points > self.continuous_buffer_size:
            raise ValueError('Duration of continuous acquisition must not exceed buffer size!')

        self.conn.write(f':SENS:DIG:FUNC "VOLTage", {scan_channels}')
        self.conn.write(f':SENS:DIG:COUN {self.continuous_one_dig_points}, {scan_channels}')
        self.conn.write(f':SENS:DIGitize:VOLTage:SRAT {rate}, {scan_channels}')
        self.conn.write(f':SENS:DIG:VOLT:RANG {self.VOLTAGE_OUT_RANGE_DIG}, {scan_channels}') #TODO should be 100 range!!!

        self.conn.write(f'ROUT:SCAN:CRE {scan_channels}')
        self.conn.write(f'ROUte:SCAN:COUN:SCAN 1')
        self.conn.write(f':ROUTe:SCAN:INTerval 0')

    def continuous_trigger(self):
        self.continuous_trigger_time = datetime.today()
        self.conn.write(':INIT')

    def continuous_abort(self):
        try:
            self.stop_auto_fetch()
        except:
            pass
        self.conn.write('ABORT')


    def continuous_fetch(self, log=True):
        if self.continuous_acquisition_type == 'all':
            query_values = 'REL, READ, CHAN'
            params_count = 3
        elif self.continuous_acquisition_type == 'single':
            query_values = 'REL, READ'
            params_count = 2
        
        samples_to = int(self.conn.query(':TRAC:ACT:END?'))
        samples_from = self.continuous_buffer_position+1
        self.continuous_buffer_position = samples_to
        logging.info(f'Fetch: {self.continuous_acquisition_type}, From: {samples_from} to: {samples_to} num of samples: {samples_to-samples_from}')

        if samples_to > samples_from: # TODO check what should happen if equal
            ret = self.conn.query_ascii_values(f'TRAC:DATA? {samples_from},{samples_to}, "defbuffer1", {query_values}')
        elif samples_to < samples_from-1:
            ret1 = self.conn.query_ascii_values(f'TRAC:DATA? {samples_from},{self.continuous_buffer_size}, "defbuffer1", {query_values}')
            ret2 = self.conn.query_ascii_values(f'TRAC:DATA? {1},{samples_to}, "defbuffer1", {query_values}')
            ret = ret1+ret2
        else:
            logging.info('Else state from continuous_fetch - samples_from equal to samples_to!!')
            return

        self.continuous_data = np.array(ret).reshape(-1, params_count)

        if self.continuous_one_selected_channel == 0:
            for el in self.continuous_data:
                el[1] *= self.V_in_SCALAR

        if self.continuous_acquisition_type == 'all':
            for row in self.continuous_data[-21:]:
                chan = int(row[2])
                self.continuous_data_all[chan] = row[1]

        if log:
            self.continuous_log(self.label)
        
        #return self.continuous_data # it seems there is no need
            

    def continuous_log(self, label=''):
        if self.continuous_acquisition_type == 'dig':
            post = f'_CH_{self.continuous_one_selected_channel}_dig'
            if label[5:9] == 'load':
                folder = 'test_load/waveforms'
            else:
                folder = 'continuous_logging_single'
        elif self.continuous_acquisition_type == 'single':
            post = f'_CH_{self.continuous_one_selected_channel}'
            folder = 'continuous_logging_single'
        else:
            post = ''
            folder = 'continuous_logging_all'
        
        self.readout_fname = f'Results/{folder}/{self.continuous_trigger_time.strftime("%Y-%m-%d %H.%M.%S.%f")[:-3]}{post}-{label}.tsv'
        try:
            open(self.readout_fname, 'a')
        except FileNotFoundError:
            Path(os.path.dirname(self.readout_fname)).mkdir(parents=True, exist_ok=True)
        with open(self.readout_fname, 'a') as f:
            for value in self.continuous_data:
                if self.continuous_acquisition_type == 'all':
                    if value[2] == 101 and not f.tell() == 0: #TODO Instead of 101 this could be the first channel from the list...
                        f.write('\n')
                    f.write(f"{value[0]:.6f}\t{value[1]:.6f}\t")
                
                elif self.continuous_acquisition_type == 'single':
                    f.write(f"{value[0]:.6f}\t{value[1]:.6f}\n")
                
                elif self.continuous_acquisition_type == 'dig':
                    f.write(f"{value[0]:.9f}\t{value[1]:.6f}\n")

    
    def match_chan(self, ch):
        '''
        match ch:
            case 101: ch_matched = 101
            case 102: ch_matched = 102
        '''

        return ch #TODO for the moment does nothing this function. Returns argument. 
    
        
    def continuous_fetch_dig(self, log=True, finish=False):
        if finish:
            return np.empty((0, 2))
        '''This function should be called only once and it will read entire buffer! '''
        samples_to = int(self.conn.query(':TRAC:ACT:END?'))
        samples_from = self.continuous_buffer_position+1
        self.continuous_buffer_position = samples_to
        logging.info(f'Fetch: dig, CH{self.continuous_one_selected_channel}, from: {samples_from} to: {samples_to}. Samples acquired: {samples_to-samples_from}')

        ret = None
        if samples_to >= samples_from:
            ret = self.conn.query_ascii_values(f'TRAC:DATA? {samples_from},{samples_to}, "defbuffer1", REL, READ')
        elif samples_to < samples_from-1:
            ret1 = self.conn.query_ascii_values(f'TRAC:DATA? {samples_from},{self.continuous_buffer_size}, "defbuffer1", REL, READ')
            ret2 = self.conn.query_ascii_values(f'TRAC:DATA? {1},{samples_to}, "defbuffer1", REL, READ')
            ret = ret1+ret2
        else: 
            logging.info('This is the case where there is no returned value in confinuous_fetch_dig')
            ret = np.empty((0, 2))
        
        
        self.continuous_data = np.array(ret).reshape(-1, 2)

        # Rescale if V_in
        if self.continuous_one_selected_channel == 0:
            for el in self.continuous_data:
                el[1] *= 43.95194394



        finish_flag=False
        
        if log:
            self.continuous_log(self.label)
        if samples_to >= self.continuous_one_dig_points:
            finish_flag=True
            #return self.continuous_fetch_dig(append=True)
        
        #return self.continuous_data + self.continuous_fetch_dig(finish=finish_flag)
        return np.append(self.continuous_data, self.continuous_fetch_dig(finish=finish_flag), axis=0)
    

            

    #Trial with multithreading

    def continuous_trigger_auto_fetch(self):
        self.continuous_trigger()
        
        interval = self.AUTO_FETCH_INTERVAL  

        self.stop_event = threading.Event()
        self.next_iteration_event = threading.Event()
        #self.instr_mutex = threading.Lock()
        self.thread = threading.Thread(target=self.repeat_function, args=(self.continuous_fetch,interval, self.stop_event, self.next_iteration_event))
        self.thread.daemon = True  # Set the thread as a daemon so it exits when the main program exits
        self.thread.start()

        self.background_measurement = True

    def stop_auto_fetch(self):
        self.stop_event.set()
        self.next_iteration_event.set()
        self.thread.join()

        self.background_measurement = False

    def _trigger_fetch_on_demand(self):
        self.next_iteration_event.set()
        for i in range(100):
            if not self.next_iteration_event.is_set():
                #print(i) # after how many ms the event occured
                break
            sleep(0.001)

    def continuous_fetch_get_latest_reading(self):
        # This could create an error if empty list returned.... To be done later
        self._trigger_fetch_on_demand()
        try:
            ret = self.continuous_data[-1][1]
        except:
            ret = -100
        return ret
    
    def continuous_fetch_get_latest_reading_all(self):
        self._trigger_fetch_on_demand()
        self.continuous_data_all = {k: v for k, v in sorted(self.continuous_data_all.items())}
        return self.continuous_data_all
        

    def repeat_function(self, func, interval, stop_event, next_iteration_event):
        while not stop_event.is_set():
            # Wait for either 1 second or the next iteration event
            result = self.next_iteration_event.wait(timeout=interval)

            #if stop_event.is_set():
            #    break
            func()
            next_iteration_event.clear()  # Clear the event flag




if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s', level=logging.INFO)

    DMM_input_voltage = DAQ6510('192.168.100.101')
    DMM_input_voltage.conn.clear()

    DMM_outputs = DAQ6510('192.168.100.102')
    DMM_outputs.conn.clear()

    current = DAQ6510('192.168.100.106')
    current.conn.clear()



    #Test of front panel instr.
    '''
    DMM_input_voltage = DAQ6510('192.168.100.101')
    
    #DMM_input_voltage.test_con()
    
    DMM_input_voltage.front_continuous_one_init_dig(0.02, rate=20000)
    sleep(0.1)
    DMM_input_voltage.continuous_trigger()

    sleep(1)
    print(DMM_input_voltage.continuous_fetch_dig())
    '''


    # Example of single channel and front panel
    '''
    DMM_input_voltage.front_continuous_one_init()
    DMM_input_voltage.continuous_trigger()

    sleep(2)
    DMM_input_voltage.continuous_fetch()
    sleep(2)

    DMM_input_voltage.continuous_abort()
    DMM_input_voltage.continuous_fetch()
    '''

    '''
    DMM_input_voltage.front_continuous_one_init()
    DMM_input_voltage.continuous_trigger_auto_fetch()

    sleep(2)
    for i in range(10):
        print(DMM_input_voltage.continuous_fetch_get_latest_reading())
        sleep(0.1)
    sleep(2)

    DMM_input_voltage.continuous_abort()
    sleep(2)
    DMM_input_voltage.continuous_fetch()
    '''






# TEST OF READ CONNECTIONS INSTR
    DMM_outputs = DAQ6510('192.168.100.102')
    #DMM_outputs.test_con()

    
    # Example with continuous all logging. 
    '''
    DMM_outputs.continuous_all_init(label='test')
    DMM_outputs.continuous_trigger()
    for i in range(9):
        DMM_outputs.continuous_fetch()
        sleep(1)
    DMM_outputs.continuous_abort()
    sleep(0.1)
    DMM_outputs.continuous_fetch()
    '''
    

    # Example with continuous single channel logging
    '''
    DMM_outputs.continuous_one_init(102, 'lab')
    DMM_outputs.continuous_trigger()
    for i in range(9):
        DMM_outputs.continuous_fetch()
        sleep(1)
    DMM_outputs.continuous_abort()
    sleep(0.1)
    DMM_outputs.continuous_fetch()
    '''

    # Example with single channel hi-speed digitize logging (for a fixed time)
    '''
    DMM_outputs.continuous_one_init_dig(102, 1, rate=400000)
    DMM_outputs.continuous_trigger()
    sleep(2)
    DMM_outputs.continuous_fetch_dig()
    '''


    #Example non-blocking
    '''
    DMM_outputs.continuous_one_init(102, 'label')
    DMM_outputs.continuous_trigger_auto_fetch()

    sleep(2)
    for i in range(10):
        print(DMM_outputs.continuous_fetch_get_latest_reading())
        #sleep(0.1)
    sleep(2)

    DMM_outputs.continuous_abort()
    sleep(2)
    DMM_outputs.continuous_fetch()
    '''
    

    # Example non-blocking all
    '''
    DMM_outputs.continuous_all_init(label='test')
    DMM_outputs.continuous_trigger_auto_fetch()

    sleep(1.2)
    for i in range(1):
        print(DMM_outputs.continuous_fetch_get_latest_reading_all())
        #sleep(0.1)
    sleep(1)

    DMM_outputs.continuous_abort()
    sleep(1)
    DMM_outputs.continuous_fetch()
    '''


    ## CURRENT MEASUREMENTS
    #current = DAQ6510('192.168.100.106')
    #current.conn.clear()
    #print(current.conn.read())
    #print(current.conn.read())
    
    #for i in range(10):
    #    print(current.front_measure_single_CURRENT())

    


    '''
    current.front_continuous_one_init_CURRENT()
    current.continuous_trigger()

    sleep(1)
    current.continuous_fetch()
    sleep(1)

    current.continuous_abort()
    current.continuous_fetch()
    '''


    '''
    current.front_continuous_one_init_dig_CURRENT(0.02, rate=1000000)
    sleep(0.1)
    current.continuous_trigger()

    sleep(1)
    print(current.continuous_fetch_dig())
    '''