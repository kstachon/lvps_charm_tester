import logging
import pyvisa
import pyvisa_py
from datetime import datetime
from time import sleep

class IT8700():
    def __init__(self, IP):
        #rm = pyvisa.ResourceManager() #alternative way
        rm = pyvisa.ResourceManager('@py')
        self.conn = rm.open_resource(f'TCPIP0::{IP}::60000::SOCKET')

        self.conn.read_termination = '\n'
        self.conn.write_termination = '\n'

        self.test_con()
        self.init()

        
    def init(self):
        self.conn.write('*RST')
        self.conn.write('SYSTem:REMote')
        
        self._initialize_to_CC_mode()
    
    def test_con(self):
        ret = self.conn.query('*IDN?')
        logging.info(f'Connected to: {ret}')
    

    def _initialize_to_CC_mode(self):
        INITIAL_VALUES = [0, 0, 0, 0, 0, 0, 0, 0]

        for ch, val in enumerate(INITIAL_VALUES):
            self.set_CC_single(ch+1, val)

    def _initialize_to_CP_mode(self):
        INITIAL_VALUES = [1, 1, 1, 1, 1, 1, 1, 1]

        for ch, val in enumerate(INITIAL_VALUES):
            self.set_CP_single(ch+1, val)
    

    def set_CC_single(self, ch, current):
        self.conn.write(f':CHAN {ch}; FUNC CURR')
        self.conn.write(f':CHAN {ch}; SOUR:CURR {current}')
    
    def set_CP_single(self, ch, power):
        self.conn.write(f':CHAN {ch}; FUNC POW')
        self.conn.write(f':CHAN {ch}; SOUR:POW {power}')

    def enable_ch_single(self, ch):
        self.conn.write(f':CHAN {ch}; INP 1')

    def enable_ch_all(self):
        self.conn.write(f'INP:ALL 1')

    def disable_ch_single(self, ch):
        self.conn.write(f':CHAN {ch}; INP 0')

    def disable_ch_all(self):
        self.conn.write(f'INP:ALL 0')

    def get_current_single(self, ch):
        self.conn.write(f':CHAN {ch}')
        ret = float(self.conn.query(':MEAS:CURR?'))
        return ret

    def get_IV_all(self):
        start_timestamp = datetime.today()

        #Temporary solution...
        '''
        results = []
        for ch in [1, 2, 2, 2, 5, 6, 7, 8]:
            results.append(self.get_current_single(ch))

        ans = {'timestamp' : start_timestamp, 
               'currents' : results,
               'voltages' : results }
        
        '''
        #TODO temporarly disabled because of broken fuckin module...
        ret = self.conn.query(':MEAS:ALLC?;ALLV?') 
        return_array = ret.split(';')
        currents = (return_array[0].split(','))
        voltages = (return_array[1].split(','))
        
        currents = [float(str) for str in currents]
        voltages = [float(str) for str in voltages]

        ans = {'timestamp' : start_timestamp, 
               'currents' : currents,
               'voltages' : voltages }
        
        return ans
    
    
    


    def __del__(self):
        self.conn.close()


if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s', level=logging.INFO)
    load = IT8700('192.168.100.111')
    #load.test_con()
    #load.enable_ch_all()
    #load.get_current_single(2)
    #load.get_IV_all()
    #load.get_IV_all()
    #load.enable_ch_single(1)

    for ch in [1, 2]:
        load.set_CC_single(ch, 0)
        load.enable_ch_single(ch)
    #sleep(2)
    print(load.get_IV_all())
    print(load.get_current_single(5))