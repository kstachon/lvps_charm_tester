import logging
import pyvisa
import pyvisa_py
from datetime import datetime

from time import sleep
import atexit

class EA_PS_9500():
    def __init__(self, IP):
        #rm = pyvisa.ResourceManager() #alternative way
        rm = pyvisa.ResourceManager('@py')
        self.conn = rm.open_resource(f'TCPIP0::{IP}::5025::SOCKET')

        self.conn.read_termination = '\n'
        self.conn.write_termination = '\n'
        #self.conn.timeout = 2000

        self.initialize()

        self.results = {
            'voltage' : -1,
            'current' : -1,
            'timestamp' : 0
        }

    def test_con(self):
        ret = self.conn.query('*IDN?')
        logging.info(ret)

    def initialize(self):
        self.test_con()
        self.conn.write('SYSTem:LOCK ON')
        self.set_limits(415, 7, 2500)
        self.set_protection(415, 7, 2500)

    def set_output(self, voltage, current, power):
        set_volt = float(voltage)
        set_curr = float(current)
        set_power = float(power)
        self.conn.write(f'VOLTage {set_volt}; CURRent {set_curr}; POWer {set_power}')
        ret = self.acknowledge_alarms_all()
        if ret == '-222,"Data out of range"':
            raise ValueError(f'Instrument returned: {ret})')

    def set_protection(self, voltage, current, power):  
        set_volt = float(voltage)
        set_curr = float(current)
        set_power = float(power)
        self.conn.write(f'VOLTage:PROTection {set_volt}; CURRent:PROTection {set_curr}; POWer:PROTection {set_power}')

    def set_limits(self, voltage, current, power):
        set_volt = float(voltage)
        set_curr = float(current)
        set_power = float(power)
        self.conn.write(f'SOURce:VOLTage:LIMit:HIGH {set_volt}; SOURce:CURRent:LIMit:HIGH {set_curr}; SOURce:POWer:LIMit:HIGH {set_power}')

    def turn_on(self):
        self.conn.write('OUTPut ON')
    def turn_off(self):
        self.conn.write('OUTPut OFF')
    
    def measure_all(self):
        ret = self.conn.query('MEAS:ARR?')
        # Split the input string by commas and spaces
        parts = ret.split(', ')
        # Extract numeric values from each part
        numeric_values = []
        for part in parts:
            value = part.split(' ')[0]
            try:
                numeric_value = float(value)
                numeric_values.append(numeric_value)
            except ValueError:
                pass
        return {'V' : numeric_values[0],
                'A' : numeric_values[1],
                'W' : numeric_values[2]
        }
        
    
    def acknowledge_alarms_all(self):
        ret = self.conn.query('SYSTem:ERRor:ALL?')
        logging.info(ret)
        return ret


if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s', level=logging.INFO)
    PS_400V = EA_PS_9500('192.168.100.105')
    PS_400V.acknowledge_alarms_all()
    
    
    #PS_400V.test_con()
    #PS_400V.set_output(20, 7, 2000)
    #PS_400V.turn_on()


    #PS_400V.turn_off()
    #print(PS_400V.measure_all()['A'])
    #print(PS_400V.measure_all()['A'])
    
    #PS_400V.turn_on()
    #sleep(1)
    #print(PS_400V.measure_all()['V'])

    #PS_400V.turn_off()
    
    #sleep(7)
    #print(PS_400V.measure_all()['A'])
    #PS_400V.turn_on()

    #sleep(1)
    #print(PS_400V.measure_all()['A'])
    
    
    #sleep(1)
    #for i in range(10):
    #    logging.info(PS_400V.measure_all())
    #    sleep(0.01)
    
    
    #PS_400V.turn_off()
    #logging.info('Turned off')
    #for i in range(50):
    #    logging.info(PS_400V.measure_all())
    #    sleep(0.01)


    