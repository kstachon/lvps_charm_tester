import logging
import pyvisa
import pyvisa_py
from datetime import datetime

class HMC8042():
    def __init__(self, IP):
        #rm = pyvisa.ResourceManager() #alternative way
        rm = pyvisa.ResourceManager('@py')
        self.conn = rm.open_resource(f'TCPIP0::{IP}::5025::SOCKET')

        self.conn.read_termination = '\n'
        self.conn.write_termination = '\n'

        self.initialize()

        self.results = {
            'voltage' : -1,
            'current' : -1,
            'timestamp' : 0
        }

    def test_con(self):
        ret = self.conn.query('*IDN?')
        logging.info(ret)

    def initialize(self):
        self.set_voltage_limit(13)
        self.set_target_voltage(12)
        self.set_target_current(1)

    def enable_interlock_power(self):
        self.enable_output()

    def disable_interlock_power(self):
        self.disable_output()

    def set_voltage_limit(self, limit):
        """
        Sets voltage limit if value positive. Otherwise, disable voltage limit functionality. 
        """
        self.conn.write("VOLT:PROT:MODE MEAS")
        if limit > 0:
            self.conn.write(f"VOLT:PROT:LEV {limit}")
            self.conn.write("VOLT:PROT ON")
        else:
            self.conn.write("VOLT:PROT OFF")

    def set_target_voltage(self, voltage):
        """
        Sets the voltage of the power supply (must be between 0V and 12V)
        """
        if not (0.0 <= voltage <= 12.0):
            raise ValueError('voltage must be between 0.0 and 12.0 (incl.)')
        self.conn.write('VOLT %f; *WAI' % voltage)

    def set_target_current(self, current):
        """
        Sets the current of the power supply (must be between 0A and 3A)
        """
        if not (0.0 <= current <= 3.0):
            raise ValueError('current must be between 0.0 and 3.0 (incl.)')
        self.conn.write('CURR %f; *WAI' % current)

    def enable_output(self):
        """
        Enables the output of the power supply
        """
        self.conn.write('OUTP ON')

    def disable_output(self):
        """
        Disables the output of the power supply
        """
        self.conn.write('OUTP OFF; *WAI')

    def get_actual_voltage(self):
        """
        Returns the actual output voltage measured by the power supply
        """
        self.last_voltage = self.conn.query_ascii_values('MEAS:VOLT?')[0]
        return self.last_voltage

    def get_actual_current(self):
        """
        Returns the actual output current measured by the power supply
        """
        self.last_current = self.conn.query_ascii_values('MEAS:CURR?')[0]
        return self.last_current
    def get_actual_VI(self):
        """
        Returns the actual output voltage and current. 
        """
        self.results['voltage'], self.results['current'] = self.get_actual_voltage(), self.get_actual_current()
        self.results['timestamp'] = datetime.today()
        return self.results  


if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)s %(levelname)s] %(message)s', level=logging.INFO)
    ps = HMC8042('192.168.100.103')
    ps.test_con()

    #ps.enable_interlock_power()
    ps.disable_interlock_power()

    print(ps.get_actual_VI())

    