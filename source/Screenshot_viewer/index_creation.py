import time
from PIL import ImageGrab

# Set the interval for capturing screenshots (in seconds)
interval = 1

while True:
    try:
        # Capture a screenshot
        screenshot = ImageGrab.grab()

        # Save the screenshot as an image file (PNG format)
        timestamp = int(time.time())
        filename = f'screenshot.png'
        screenshot.save(filename, 'PNG', quality=95)

        print(f'Screenshot saved as {filename}')

        # Wait for the next interval
        time.sleep(interval)
    except KeyboardInterrupt:
        # Exit the loop if Ctrl+C is pressed
        break