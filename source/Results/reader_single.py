import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np

# Define the file path
file_path = 'continuous_logging_single/2023-09-10 23.44.56.579_CH_101_dig-on_off_CH_101.tsv'
#file_path2 = 'continuous_logging_single/2023-09-05 11.19.33.898_CH_201_dig-test_load_10V_10.tsv'

# Define the columns you want to extract and plot
selected_columns = ['Time', 'Voltage']


def read_and_plot(file_path):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(file_path, delimiter='\t', names=['Time', 'Voltage'])

    voltage_values = df['Voltage'].values
    mean = np.mean(voltage_values)
    ptp = np.ptp(voltage_values)
    rms = math.sqrt(sum((voltage-mean)**2 for voltage in voltage_values) / len(voltage_values))
    print(f'RMS value: {rms}')
    print(f'Peak-to-peak value: {ptp}')

    # Create an interactive plot
    fig, ax = plt.subplots()
    ax.set_title('Selected Columns Plot')
    
    # Plot each selected column
    lines = {}
    for col in selected_columns[1:]:
        lines[col], = ax.plot(df['Time'], df[col], label=col)
    
    ax.legend()
    ax.set_xlabel('Time')
    ax.set_ylabel('Values')

    # Enable interactive mode
    plt.ion()
    plt.show()

    try:
        while True:
            # Update the plot
            for col in selected_columns[1:]:
                lines[col].set_ydata(df[col])
            
            fig.canvas.flush_events()
            
            # Pause for a short duration (e.g., 1 second)
            plt.pause(1)
    
    except KeyboardInterrupt:
        # Close the plot on keyboard interrupt (Ctrl+C)
        plt.ioff()
        plt.close()

if __name__ == "__main__":
    read_and_plot(file_path)