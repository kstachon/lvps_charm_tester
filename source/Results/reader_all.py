import pandas as pd
import matplotlib.pyplot as plt

# Define the file path
file_path = 'test_monitor_all/2023-09-05 09.18.49.710.tsv'

# Define the columns you want to extract and plot
selected_columns = ['time', 'V_in', 'I_in', 'V_out_301','V_out_302','V_out_303', 'I_out_301', 'I_out_302', 'I_out_303']

def read_and_plot(file_path, selected_columns):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(file_path, delimiter='\t', parse_dates=['time'])

    # Select the desired columns
    df = df[selected_columns]

    # Create an interactive plot
    fig, ax = plt.subplots()
    ax.set_title('Selected Columns Plot')
    
    # Plot each selected column
    lines = {}
    for col in selected_columns[1:]:
        lines[col], = ax.plot(df['time'], df[col], label=col)
    
    ax.legend()
    ax.set_xlabel('Time')
    ax.set_ylabel('Values')

    # Enable interactive mode
    plt.ion()
    plt.show()

    try:
        while True:
            # Update the plot
            for col in selected_columns[1:]:
                lines[col].set_ydata(df[col])
            
            fig.canvas.flush_events()
            
            # Pause for a short duration (e.g., 1 second)
            plt.pause(1)
    
    except KeyboardInterrupt:
        # Close the plot on keyboard interrupt (Ctrl+C)
        plt.ioff()
        plt.close()

if __name__ == "__main__":
    read_and_plot(file_path, selected_columns)