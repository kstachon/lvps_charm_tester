# simple CAN bus access using python
# see https://pypi.org/project/python-can/
# see https://python-can.readthedocs.io/en/stable/

import sys
from time import sleep
from datetime import datetime
from struct import *
import enum
import logging

import math

# WIENER-Specific modules
from CAN.wiener_can.can_v1 import *
from CAN.wiener_can.can import Can, Can_V1

def CalculateRt(adc):
    """ Helper: Calculate Resistor form voltage divider.
        adc: measured adc value (0...ADC_MAX-1)
        R1: Resistor from ADC-REF to ADC-Input
        Rt: Resistor from ADC-Input to GND
    """
    ADC_MAX = 4096      # 12 bit ADC max value
    R1 = 1000.

    if(ADC_MAX == adc):
        adc = ADC_MAX-1     # should not be possible, but be save...
    Rt = R1 * adc / (ADC_MAX-adc)
    return Rt

def CalculateTemperatureFromKTY(Rt):
    """ Helper: Convert temperature voltage to resistance
        See work51\MUH6\lib51\source\MAKETEMP.CPP
    """
    A = 0.0195139
    B = 178.6655
    C = 190.57

    t = math.sqrt((Rt-C)/A) - B
    return t
    




class UCTest:
    """Class holding add data and fuctions for the CMS CHARM Microcontroller Test"""
    def __init__(self):
        # Data received from DUT (negative number => never read from DUT)
        self.uninit_DUT_vars()

        # internal variables
        self.log_line = 0
        self.can_uc = Can_V1()

        self.DEF = None

    def uninit_DUT_vars(self):
        # Data received from DUT (negative number => never read from DUT)
        self.IdleLoopCount      = -1
        self.RamErrorCount      = -1
        self.CanErrorCount      = -1
        self.ChecksumA          = -1
        self.ChecksumDCB        = -1
        self.RamInitErrorsSmall  = -1
        self.RamInitErrorsLarge  = -1
        self.RamWriteErrorsSmall = -1
        self.RamWriteErrorsLarge = -1

    def get_channel_measure_0(self, module_number, channel_number, voltage_exponent = -10, current_exponent = -9):
        """ Get measure0 data from module and channel """
        
        can_rd_m0 = self.can_uc.can_read_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.ReadMeas0, (module_number << 8) + channel_number)
        if(can_rd_m0 is not None):
            status, uSense, iModule, reserved = unpack('HhhH', can_rd_m0)                                  # convert result to integer
            uSense = math.ldexp(uSense, voltage_exponent)
            iModule = math.ldexp(iModule, current_exponent)
            #print("Meas0(module {0}, channel {1}): ChannelStatus: {2}, uSense: {3}, iSense:{4}, reserved:{5}".format(module_number, channel_number, status, uSense, iModule, reserved))
            #print("Meas0(module {0}, channel {1}): ChannelStatus: {2}, uSense: {3}, iSense:{4}".format(module_number, channel_number, status, uSense, iModule))
            #return [status, uSense*2.2, iModule*2.7 - 2.66]
            return (status, uSense, iModule)
        else:
            raise ValueError()
        

    def get_channel_measure_1(self, module_number, channel_number):
        """ Get measure0 data from module and channel """
        
        can_rd_m1 = self.can_uc.can_read_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.ReadMeas1, (module_number << 8) + channel_number)
        if(can_rd_m1 is not None):
            uSetReadBack, iSetReadBack, reserved2, reserved3 = unpack('HHHH', can_rd_m1)                                  # convert result to integer
            uSetReadBack = uSetReadBack >> 4                # 16 bit => 12 bit ADC values
            iSetReadBack = iSetReadBack >> 4
            #print("Meas1(module {0}, channel {1}): uSetReadBack: {2}, iSetReadBack: {3}, reserved:{4}, reserved:{5}".format(module_number, channel_number, uSetReadBack, iSetReadBack, reserved2, reserved3))
            #print("Meas1(module {0}, channel {1}): uSetReadBack: {2}, iSetReadBack: {3}".format(module_number, channel_number, uSetReadBack, iSetReadBack))
            return (uSetReadBack, iSetReadBack)
        else:
            raise ValueError()
        

    def get_module_measure_0(self, module_number, voltage_exponent = -6):
        """ Get measure0 data from module """
        
        channel_number = 255        # address the module part
        can_rd_m0 = self.can_uc.can_read_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.ReadMeas0, (module_number << 8) + channel_number)
        if(can_rd_m0 is not None):
            status, uInput, temp1, temp2  = unpack('HHHh', can_rd_m0)                                  # convert result to integer
            uInput = math.ldexp(uInput, voltage_exponent)

            Rt = CalculateRt(temp1)
            temp1 = CalculateTemperatureFromKTY(Rt)

            Rt = CalculateRt(temp2)
            temp2 = CalculateTemperatureFromKTY(Rt)

            #print("Meas0(module {0}): ModuleStatus: {1}, temp1: {2}, temp2:{3}, uInput:{4}".format(module_number, status, temp1, temp2, uInput))
            return (status, temp1, temp2, uInput)
        else:
            raise ValueError()

    def get_module_measure_1(self, module_number, voltage_exponent = -6):
        """ Get measure1 data from module """
      
        channel_number = 255        # address the module part
        can_rd_m1 = self.can_uc.can_read_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.ReadMeas1, (module_number << 8) + channel_number)
        if(can_rd_m1 is not None):
            uAux, reserved1, reserved2, reserved3 = unpack('hHHH', can_rd_m1)                                  # convert result to integer
            uAux = math.ldexp(uAux, voltage_exponent)

            #print("Meas1(module {0}): uAux: {1}, reserved1: {2}, reserved2:{3}, reserved3:{4}".format(module_number, uAux, reserved1, reserved2, reserved3))
            #print("Meas1(module {0}): uAux: {1}".format(module_number, uAux))
            return uAux
        else:
            raise ValueError()



    def get_counter_idle_ram(self, module_number):
        """ Get IdleLoopCount and RamErrorCount from DUT.
        This is done via get_module_measure_3,
        """
        
        channel_number = 255        # address the module part
        can_rd_m3 = self.can_uc.can_read_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.ReadMeas3, (module_number << 8) + channel_number)
        if(can_rd_m3 is not None):
            self.IdleLoopCount, self.RamErrorCount = unpack('LL', can_rd_m3)                                  # convert result to integer
            #print("Meas3(module {0}): IdleLoopCount: {1}, RamErrorCount: {2}".format(module_number, self.IdleLoopCount, self.RamErrorCount))
            return (self.IdleLoopCount, self.RamErrorCount)
        else:
            raise ValueError()
       


    #     Using RTR command RD0, channel 0
    #     """
    #     can_rd = self.can_uc.can_read_transaction(CAN_V1_CMDID.RD0, 0)
    #     if(can_rd is not None):
    #         self.IdleLoopCount, self.RamErrorCount = unpack('LL', can_rd)                                  # convert result to integer
    #         #print("Meas0: IdleLoopCount: {0}, RamErrorCount: {1}".format(self.IdleLoopCount, self.RamErrorCount))

    # def get_counter_can(self):
    #     """ Get CanErrorCount from DUT.

    #     Using RTR command RD0, channel 1
    #     """
    #     can_rd = self.can_uc.can_read_transaction(CAN_V1_CMDID.RD0, 1)
    #     if(can_rd is not None):
    #         self.CanErrorCount, tbd = unpack('LL', can_rd)                                  # convert result to integer
    #         #print("Meas0+1: CanErrorCount: 0x{0:04X}, tbd: 0x{1:04X}".format(self.CanErrorCount, tbd))

    # def get_flash_checksums(self):
    #     """ Get Flash ChecksumA and ChecksumDCB from DUT.

    #     Test firmware flash (duration: 170ms).
    #     Using RTR command RD1, channel 0
    #     """
    #     can_rd = self.can_uc.can_read_transaction(CAN_V1_CMDID.RD1, 0)
    #     if(can_rd is not None):
    #         self.ChecksumA, self.ChecksumDCB = unpack('LL', can_rd)                                  # convert result to integer
    #         #print("Meas1: Flash ChecksumA: A: 0x{0:04X}, ChecksumDCB: 0x{1:04X}".format(self.ChecksumA, self.ChecksumDCB))

    # def get_ram_initial_changes(self):
    #     """ Compare RAM with initial value, get RamInitErrorSmall and RamInitErrorLarge from DUT.

    #     Test fitest rd2 (duration: 40ms).
    #     Using RTR command RD2, channel 0
    #     """
    #     can_rd = self.can_uc.can_read_transaction(CAN_V1_CMDID.RD2, 0)
    #     if(can_rd is not None):
    #         self.RamInitErrorsSmall, self.RamInitErrorsLarge = unpack('LL', can_rd)                                  # convert result to integer
    #         #print("Meas2: RamInitErrorsSmall: 0x{0:04X}, RamInitErrorsLarge: 0x{1:04X}".format(self.RamInitErrorsSmall, self.RamInitErrorsLarge))


    # def get_ram_write_test(self):
    #     """ Do RAM write test, get RamWriteErrorsSmall and RamWriteErrorsLarge from DUT.

    #     Test test rd3 (duration: 60ms)
    #     Using RTR command RD3, channel 0
    #     """
    #     can_rd = self.can_uc.can_read_transaction(CAN_V1_CMDID.RD3, 0)
    #     if(can_rd is not None):
    #         self.RamWriteErrorsSmall, self.RamWriteErrorsLarge = unpack('LL', can_rd)                                  # convert result to integer
    #         #print("Meas3: RamWriteErrorsSmall: 0x{0:04X}, RamWriteErrorsLarge: 0x{1:04X}".format(self.RamWriteErrorsSmall, self.RamWriteErrorsLarge))


    def set_channel_on_off(self, module_number, channel_number, switch_on = True):
        """ Switch Module Channel on or off """
        service_code = CAN_V2_SERVICE_CODE_TYPE.WriteCmd
        if ( switch_on):
            command_code =  CAN_V2_CMD0_CODE.MODULE_ON
        else:
            command_code =  CAN_V2_CMD0_CODE.MODULE_OFF

        # creat e a bytearray
        data = pack("h", command_code)

        can_rd_m0 = self.can_uc.can_write_command_transaction_v2(CAN_V2_SERVICE_CODE_TYPE.WriteCmd, (module_number << 8) + channel_number, data)
        if(can_rd_m0 is not None):
            rx_command_code, command_result = unpack('HH', can_rd_m0)                                  # convert result to integer
            return ("WriteCmd(module {0}, channel {1}): service_code {2}, command_code: {3} => command_code: {4} status {5}"
                  .format(module_number, channel_number, service_code, command_code, rx_command_code, command_result))



    def set_channel_ocp(self, module_number, channel_number, ocp_value, current_exponent = -9):
        #TODO scaling for prototype
        #ocp_value = ocp_value/12
        """ Set Module Channel OCP (Over Current Protection) value """
        value_i16 = round(math.ldexp(ocp_value, -current_exponent))
        offset = 18                                 # __offsetof(MARAPOD_CHANNEL_USER_CONFIG_DATA, monitoringLimits.maxCurrent)

        address = CAN_EXTID_AREA.STD_USERCONFIG
        address += offset
        service_code = CAN_V2_SERVICE_CODE_TYPE.WriteMem

        # creat e a bytearray
        data = pack("HH", address, value_i16)

        can_rd_m0 = self.can_uc.can_write_command_transaction_v2(service_code, (module_number << 8) + channel_number, data)
        if(can_rd_m0 is not None):
            rx_address, command_result = unpack('HH', can_rd_m0)                                  # convert result to integer
            return ("OCP: WriteMem(module {0}, channel {1}): service_code {2}, address: {3}, data: {4} => address: {5} status {6}"
                  .format(module_number, channel_number, service_code, address, value_i16, rx_address, command_result))


    def set_channel_u(self, module_number, channel_number, u_value, voltage_exponent = -10):
        #TODO scaling for prototype
        #u_value = u_value/16
        """ Set Module Channel Voltage value """
        value_i16 = round(math.ldexp(u_value, -voltage_exponent))
        offset = 2                                 # offsetof(MARAPOD_CHANNEL_USER_CONFIG_DATA, unom)

        address = CAN_EXTID_AREA.STD_USERCONFIG
        address += offset
        service_code = CAN_V2_SERVICE_CODE_TYPE.WriteMem

        # creat e a bytearray
        data = pack("HH", address, value_i16)

        can_rd_m0 = self.can_uc.can_write_command_transaction_v2(service_code, (module_number << 8) + channel_number, data)
        if(can_rd_m0 is not None):
            rx_address, command_result = unpack('HH', can_rd_m0)                                  # convert result to integer
            return ("USET: WriteMem(module {0}, channel {1}): service_code {2}, address: {3}, data: {4} => address: {5} status {6}"
                  .format(module_number, channel_number, service_code, address, value_i16, rx_address, command_result))



    def switch_channel_on_with_values(self, module_number, channel_number, ocp_value, u_value, current_exponent=-9):
        """ Combination of off, set OCP, set USET, ON """

        # switch off
        self.set_channel_on_off(module_number, channel_number, False )
        sleep(0.001)
        self.set_channel_ocp(module_number, channel_number, ocp_value, current_exponent)
        sleep(0.001)
        # set USET
        self.set_channel_u(module_number, channel_number, u_value)
        sleep(0.001)
        # switch on
        self.set_channel_on_off(module_number, channel_number, True )
        sleep(0.005)


    def log(self, file_logfile, field_separator = "\t"):
        """Generate LOG file entry.

        Format: ASCII
        Field separator: field_separator
        Fields:
            Timestamp: Date / Time in ISO-Format, millisecond resolution (YYYY-MM-DD HH:MM:SS.mmm)
            error_timeout_counter
            error_transaction_counter
            statistics_transaction_counter
            IdleLoopCount
            RamErrorCount
            CanErrorCount
            ChecksumA
            ChecksumDCB
            RamInitErrorsSmall
            RamInitErrorsLarge
            RamWriteErrorsSmall
            RamWriteErrorsLarg
        """
        timestamp = datetime.today().isoformat(sep=' ', timespec='milliseconds')

        # debug
        #can_statistics = "{1}{0} TX:{2}, RX:{3}, TR:{4} ETX:{5}, ERX:{6}, ETR:{7}, ETI:{8} ".format(field_separator,
        #                                  timestamp,
        #                                  self.can_uc.statistics_tx_counter,
        #                                  self.can_uc.statistics_rx_counter,
        #                                  self.can_uc.statistics_transaction_counter,
        #                                  self.can_uc.error_tx_counter,
        #                                  self.can_uc.error_rx_counter,
        #                                  self.can_uc.error_transaction_counter,
        #                                  self.can_uc.error_timeout_counter
        #                                  )
        #print("{0}{1}".format(timestamp, can_statistics))

        if(self.log_line == 0):
            log_string_head = "{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}\n".format(field_separator,
                                              "timestamp",
                                              "error_timeout_count",
                                              "error_transaction_count",
                                              "statistics_transaction_count",
                                              "IdleLoopCount",
                                              "RamErrorCount",
                                              "CanErrorCount",
                                              "ChecksumA",
                                              "ChecksumDCB",
                                              "RamInitErrorsSmall",
                                              "RamInitErrorsLarge",
                                              "RamWriteErrorsSmall",
                                              "RamWriteErrorsLarge"
                                              )
            #print(log_string_head, end='')
            file_logfile.write(log_string_head)

        self.log_line += 1
        log_string = "{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}\n".format(field_separator,
                                          timestamp,
                                          self.can_uc.error_timeout_counter,
                                          self.can_uc.error_transaction_counter,
                                          self.can_uc.statistics_transaction_counter,
                                          self.IdleLoopCount,
                                          self.RamErrorCount,
                                          self.CanErrorCount,
                                          self.ChecksumA,
                                          self.ChecksumDCB,
                                          self.RamInitErrorsSmall,
                                          self.RamInitErrorsLarge,
                                          self.RamWriteErrorsSmall,
                                          self.RamWriteErrorsLarge
                                          )
        #print(log_string, end='')
        #print(".", end='', flush=True)
        print("..........\n", end='', flush=True)
        file_logfile.write(log_string)
        file_logfile.flush()






###################################################################################################
def main():
    logging.basicConfig(level=logging.CRITICAL)
    import keyboard

    # Log File base name
    logfile_base = "CHARM-uC"

    # Output Log file
    log_timestamp = datetime.today().strftime('%Y%m%d-%H%M%S')
    logfile = logfile_base + "-" + log_timestamp + ".txt"
    #logfile = logfile_base  + ".txt"
    file_logfile = open(logfile, "w")

    # Creat the Class used to hold all test data
    uc_test = UCTest()

    # MARAPOD module number to be tested (0..127)
    module_number = 127
    # MARAPOD channel number to be tested (0..11)
    channel_number = 0

    # initial voltage value
    u_value = 8

    # keyboard handler to abort the infinite main loop
    execute_loop: bool = True
    def handler_ESC():
        nonlocal execute_loop
        execute_loop = False
    keyboard.add_hotkey('ESC', handler_ESC, suppress=True)
    print ("Hit 'ESC' to stop")

    # the following loop is executed once a second
    second_counter: int = 0
    while execute_loop:
        # set all DUT vars to invalid value
        uc_test.uninit_DUT_vars()

        # get DUT status counters (old thing from CHARM eval board test, April)
        # uc_test.get_counter_idle_ram()
        # uc_test.get_counter_can()

        # get DUT channel measurement data
        for channel_number in range(12):
            uc_test.get_channel_measure_0(module_number, channel_number)
            uc_test.get_channel_measure_1(module_number, channel_number)

        # get DUT module measurement data
        uc_test.get_module_measure_0(module_number)            
        uc_test.get_module_measure_1(module_number)
        uc_test.get_counter_idle_ram(module_number)             # get_module_measure_3

        sleep(4)


        # switch all off
        for channel_number in range(12):
            uc_test.set_channel_on_off(module_number, channel_number, False )

        sleep(1)


        # U to 8 V or 10 V
        if(u_value != 8):
            u_value = 8
        else:
            u_value = 10

        for channel_number in range(12):
            ocp_value = 5
            uc_test.switch_channel_on_with_values(module_number, channel_number, ocp_value, u_value)


        # for channel_number in range(12):
        #     # OCP to 5A
        #     ocp_value = 5
        #     uc_test.set_channel_ocp(module_number, channel_number, ocp_value)
        #     # set USET
        #     uc_test.set_channel_u(module_number, channel_number, u_value)
        #     # switch on
        #     uc_test.set_channel_on_off(module_number, channel_number, True )




        # execute specific tests
        # uc_test.get_ram_initial_changes()

        #once a minute, so every 60 seconds
        # if((second_counter % 60) == 30):
        #     uc_test.get_flash_checksums()

        # once a hour, so every 3600 seconds
        # if((second_counter % 3600) == 1800):
        #     uc_test.get_ram_write_test()

        uc_test.log(file_logfile)
        sleep(1)
        second_counter += 1

    file_logfile.close()
    print("byby")



if(__name__ == "__main__"):
    main()
