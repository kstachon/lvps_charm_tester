""" CAN V1 Protocol Definitions.

Original source: See moduleCom.h (SVN)
"""

class CAN_V1_CMDID:
    """ SUB-ID's for communication.
    """
    SYNC  = 0     # SYNC, serial number access
    OFF   = 1     # Modules send SwitchOff-Command
    RDCMD = 2     # read memory / command buffer
    WRCMD = 3     # write memory / command buffer
    RD0   = 4     # measure data block 0..3
    RD1   = 5
    RD2   = 6
    RD3   = 7


class CAN_V2_SERVICE_CODE_TYPE:
    """Enumaration for holding valid service codes for the wiener can
       protocol version 2
    """
    Nop         = 0x000
    # Read service codes
    WriteCmd    = 0x002
    WriteMem    = 0x004
    WriteObj    = 0x006
    # Read service codes
    ReadMem     = 0x080
    ReadObj     = 0x082
    # Read measure service codes
    ReadMeas0   = 0x100
    ReadMeas1   = 0x102
    ReadMeas2   = 0x104
    ReadMeas3   = 0x106
    MAX         = 0xFFF


class CAN_V2_CMD0_CODE:
    """Enumaration for holding valid command codes for the wiener can
       protocol version 2
    """
    MODULE_ON   = 0x0001    # General application control command
    MODULE_OFF  = 0x0002    # General application control command 


class CAN_EXTID_AREA:
    STD_FIRMWARE     = 0x00000000     # Read fimrware identifier
    STD_SYSCONFIG    = 0x00001000     # Read/Write access to device system config data
    STD_USERCONFIG   = 0x00002000     # Read/Write access to device user config data
    STD_EXTEEPROM    = 0x0000C000     # Read/Write access to external EEPROM attached to the device (i.e. BIN EEPROM) 
    STD_FWTYPEINFO   = 0x0000D000     # Read access to currently running firmware type (Application or bootloader, see defines for details)
    STD_FLASHBUFFER  = 0x0000E000     # Read/Write access to RAM buffer used as intermediate for flash access
    STD_TRACE        = 0x0000F000     # Read/Write access to RAM buffer used for device-specific trace functions (i.e. ADC value)
    STD_SENDACKN     = 0x00010000     # Write access: Request aknowledge answer
