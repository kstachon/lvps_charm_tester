""" Low Level CAN access functions.
"""
import can
import sys
import time
import logging


# WIENER-Specific modules
#from wiener_can.can_v1 import *

class Can:
    """ CAN-Bus communication.
    """
    def __init__(self):
        self.bus = None
        self.txTimeout = 1 #TODO changed this from 1
        self.rxTimeout = 1 #TODO changed this from 1
        self.bitrate = 125000
        self.retries = 0

        # error counter
        self.error_tx_counter = 0               # incremented each time an error occurwed during transmission
        self.error_rx_counter = 0               # incremented each time an error occurwed during receprion
        self.error_transaction_counter = 0      # transaction failed ( no response to CAN request message)
        self.is_last_transaction_ok = True      # Used to detect the transition "successfull transaction" -> "error state"
        self.error_timeout_counter  = 0         # incremented when successful transaction -> error

        self.error_frame_counter = 0

        # Internal performance / statistic counter
        self.statistics_tx_counter = 0          # incremented each time a CAN message is transmitted
        self.statistics_rx_counter = 0          # incremented each time a CAN message is received
        self.statistics_transaction_counter = 0 # incremented each time a CAN message is transmitted and the corresponding response is received

        self.can_init()

    def __del__(self):
        self.bus.shutdown()        


    def can_init(self):
        """ Initialize CAN interface for communication with 125000 bit.

        The following parameters are defined in th econfig file can.ini:
        interface = pcan
        channel = PCAN_USBBUS1
        """
        try:
            self.bus = can.interface.Bus(bitrate = self.bitrate
                                    #, auto_reset = True
                                    )
        except can.CanInterfaceNotImplementedError as err:
            logging.critical("CanInterfaceNotImplementedError: Interface isn’t recognized or cannot be loaded.")
            sys.exit()
        except can.CanInitializationError as err:
            logging.critical("CanInitializationError: Could not convert data to an integer.")
            sys.exit()
        except ValueError as err:
            logging.critical("ValueError: channel could not be determined.")
            sys.exit()
        except Exception as err:
            logging.critical(f"Unexpected {err=}, {type(err)=}")
            sys.exit()
        else:
            logging.info('Opened {0} (device number {2}), {1}, Status {3}'
                  .format(self.bus.channel_info, self.bus.state, self.bus.get_device_number(), self.bus.status_string() ))

    def is_response_message(self, txMsg, rxMsg) -> bool:
        """ Return True if rxMsg is a valid response of txMsg.
        """
        if ((txMsg == None) or (rxMsg == None)):            # e.g. invalid tx message, rx timeout
            return False
        if(rxMsg.is_remote_frame):                          # Response may not be an remote request
            return False
        if(txMsg.is_extended_id != rxMsg.is_extended_id):   # both messages must be extended or not extended
            return False
        if(txMsg.is_remote_frame):                          # RTR request: ID must be identical
            if(txMsg.arbitration_id != rxMsg.arbitration_id):
                return False
            # TODO: check the byte count ?
            return True
        
        # Default: we don't know the message, so assume no response
        return False


    def can_wrd_transaction(self, txMsg) -> bytearray:
        """Send CAN Message (11 bit ID) and return the result.

        param txMsg      CAN message to be sent
        param timeout
        param retries
        retval None Any error
        retval The received CAN message
        todo check if the received message is the response of the tx message
        todo additional parameter , *, max_retries = 0 
        todo addtitional parameter timeout
        """

        self.bus.flush_tx_buffer()          # ignore any reminding messages 

        if(not self.bus.status_is_ok()):    # any error => re-init
            logging.warning ("CAN Status {0}    => reset".format(self.bus.status_string()))
            self.bus.shutdown()
            self.can_init()

        # send can message
        try:
            self.bus.send(txMsg, timeout = self.txTimeout)
            logging.debug ('can_wrd_transaction:tx %s', txMsg.__str__())
        except can.CanOperationError as err:
            logging.warning ('tx ERR CanOperationError Code "{0}"'.format(err.error_code))
            self.error_tx_counter += 1
            self.error_transaction_counter += 1
            if(self.is_last_transaction_ok):
                self.error_timeout_counter += 1
            self.is_last_transaction_ok = False
            return None
        except can.CanError as err:
            logging.warning ('tx ERR CanError Code {0}, MSG "{1}"'.format(err.error_code, txMsg.__str__()))
            self.error_tx_counter += 1
            self.error_transaction_counter += 1
            if(self.is_last_transaction_ok):
                self.error_timeout_counter += 1
            self.is_last_transaction_ok = False
            return None
        self.statistics_tx_counter += 1

        # read next received message
        rx_start_time = time.perf_counter()                # remember the transaction start time
        rx_timeout = self.rxTimeout

        # read all received messages until the expected response has been received
        while True:
            try:
                rxMsg = self.bus.recv(timeout = 0.1)
            except can.CanOperationError:
                logging.error ("bus.recv: CanOperationError")
                self.error_rx_counter += 1
                self.error_transaction_counter += 1
                if(self.is_last_transaction_ok):
                    self.error_timeout_counter += 1
                self.is_last_transaction_ok = False
                return None

            if (not rxMsg == None):     # ignore timeout here
                if (rxMsg.is_error_frame):
                    #logging.debug ("rx Error Frame, Status {0}".format(self.bus.status_string()))
                    self.error_frame_counter = self.error_frame_counter+1
                else:
                    # CAN message received
                    self.statistics_rx_counter += 1
                    if(self.is_response_message(txMsg, rxMsg)):
                        # Return the message if it is the expected one. Else the message is ignored
                        logging.debug ('can_wrd_transaction:rx: %s', rxMsg.__str__())
                        self.statistics_transaction_counter += 1
                        self.is_last_transaction_ok = True
                        return rxMsg.data

            # check if we should wait for the correct message, or if we have a timeout
            if( (rx_elapsed_time := time.perf_counter()-rx_start_time) > rx_timeout):
                break
           
        logging.warning ("bus.recv: Timeout (%.3f > %.3f)", rx_elapsed_time, rx_timeout)
        self.error_transaction_counter += 1
        if(self.is_last_transaction_ok):
            self.error_timeout_counter += 1
        self.is_last_transaction_ok = False
        return None



    def is_response_message_v2(self, txMsg, rxMsg) -> bool:
        """ Return True if rxMsg is a valid response of txMsg.
        """
        if ((txMsg == None) or (rxMsg == None)):            # e.g. invalid tx message, rx timeout
            return False
        if(rxMsg.is_remote_frame):                          # Response may not be an remote request
            return False
        if(txMsg.is_extended_id != rxMsg.is_extended_id):   # both messages must be extended or not extended
            return False
        if(txMsg.is_remote_frame):                          # RTR request: ID must be identical
            if(txMsg.arbitration_id != rxMsg.arbitration_id):
                return False

#        if(rxMsg.dlc != 8):
#            return False

        tx_id = txMsg.arbitration_id
        rx_id = rxMsg.arbitration_id
        rx_id += 0x00010000
        if(tx_id == rx_id):
            return True
        
        # Default: we don't know the message, so assume no response
        return False





    def can_wrd_transaction_v2(self, txMsg) -> bytearray:
        """Send CAN Message (29 bit ID) and return the result.

        param txMsg      CAN message to be sent
        param timeout
        param retries
        retval None Any error
        retval The received CAN message
        todo check if the received message is the response of the tx message
        todo additional parameter , *, max_retries = 0 
        todo addtitional parameter timeout
        """

        self.bus.flush_tx_buffer()          # ignore any reminding messages 

        if(not self.bus.status_is_ok()):    # any error => re-init
            logging.warning ("CAN Status {0}    => reset".format(self.bus.status_string()))
            
            # TODO removed to speed up radiation test
            self.bus.shutdown()
            self.can_init()
           

        # send can message
        try:
            self.bus.send(txMsg, timeout = self.txTimeout)
            logging.debug ('can_wrd_transaction:tx %s', txMsg.__str__())
        except can.CanOperationError as err:
            logging.warning ('tx ERR CanOperationError Code "{0}"'.format(err.error_code))
            self.error_tx_counter += 1
            self.error_transaction_counter += 1
            if(self.is_last_transaction_ok):
                self.error_timeout_counter += 1
            self.is_last_transaction_ok = False
            return None
        except can.CanError as err:
            logging.warning ('tx ERR CanError Code {0}, MSG "{1}"'.format(err.error_code, txMsg.__str__()))
            self.error_tx_counter += 1
            self.error_transaction_counter += 1
            if(self.is_last_transaction_ok):
                self.error_timeout_counter += 1
            self.is_last_transaction_ok = False
            return None
        self.statistics_tx_counter += 1

        # read next received message
        rx_start_time = time.perf_counter()                # remember the transaction start time
        rx_timeout = self.rxTimeout

        # read all received messages until the expected response has been received
        while True:
            try:
                rxMsg = self.bus.recv(timeout = 0.1)
            except can.CanOperationError:
                logging.error ("bus.recv: CanOperationError")
                self.error_rx_counter += 1
                self.error_transaction_counter += 1
                if(self.is_last_transaction_ok):
                    self.error_timeout_counter += 1
                self.is_last_transaction_ok = False
                return None

            if (not rxMsg == None):     # ignore timeout here
                if (rxMsg.is_error_frame):
                    #logging.debug ("rx Error Frame, Status {0}".format(self.bus.status_string()))
                    self.error_frame_counter = self.error_frame_counter+1
                else:
                    # CAN message received
                    self.statistics_rx_counter += 1
                    if(self.is_response_message_v2(txMsg, rxMsg)):
                        # Return the message if it is the expected one. Else the message is ignored
                        logging.debug ('can_wrd_transaction:rx: %s', rxMsg.__str__())
                        self.statistics_transaction_counter += 1
                        self.is_last_transaction_ok = True
                        return rxMsg.data

            # check if we should wait for the correct message, or if we have a timeout
            if( (rx_elapsed_time := time.perf_counter()-rx_start_time) > rx_timeout):
                break
           
        logging.warning ("bus.recv: Timeout (%.3f > %.3f)", rx_elapsed_time, rx_timeout)
        self.error_transaction_counter += 1
        if(self.is_last_transaction_ok):
            self.error_timeout_counter += 1
        self.is_last_transaction_ok = False
        return None






class Can_V1(Can):
    """ Can Protocol V1 specific functions.
    """
    def can_read_transaction(self, idCmd, idModGrp = 0x00, isGroupCommand = False) -> bytearray:
        """Send RTR-Message (11 bit ID) and return the result.

        param idCmd      CAN_V1 command (0...7)
        param idModGrp   CAN_V1 module / group id (0..127)
        param isGroupCommand   False (default) if single module command, True if group command
        param timeout
        returns If any error: 
        """
        # prepare RTR message to be sent
        if(not isGroupCommand):
            idModGrp |= 0x80

        txMsg = can.Message(
            is_extended_id = False,
            arbitration_id = (idCmd << 8) + idModGrp,
            dlc = 8,
            is_remote_frame = True
        )
        return self.can_wrd_transaction(txMsg)

    def can_read_transaction_v2(self, service_code, node_id) -> bytearray:
        """Quick function to allow v1 and v2 in parrallel, to be replaced by a CAN_V2 class"""
        """Send RTR-Message (11 bit ID) and return the result.

        param idCmd      CAN_V1 command (0...7)
        param idModGrp   CAN_V1 module / group id (0..127)
        param isGroupCommand   False (default) if single module command, True if group command
        param timeout
        returns If any error: 
        """
        txMsg = can.Message(
            is_extended_id = True,
            #arbitration_id = (idCmd << 8) + idModGrp,
            arbitration_id = ((0x1000 | (service_code+1)) << 16) + node_id,
            dlc = 0,
            is_remote_frame = False
        )
        return self.can_wrd_transaction_v2(txMsg)


    def can_write_command_transaction_v2(self,  service_code, node_id, mydata) -> bytearray:
        """Quick function to allow v1 and v2 in parrallel, to be replaced by a CAN_V2 class"""
        """Send RTR-Message (11 bit ID) and return the result.

        param idCmd      CAN_V1 command (0...7)
        param idModGrp   CAN_V1 module / group id (0..127)
        param isGroupCommand   False (default) if single module command, True if group command
        param timeout
        returns If any error: 
        """
        testlen = len(mydata)

        txMsg = can.Message(
            is_extended_id = True,
            arbitration_id = ((0x1000 | (service_code+1)) << 16) + node_id,
            dlc = len(mydata),                # 2 byte command code, 2 byte command parameter
            is_remote_frame = False,
            data = mydata
        )
        return self.can_wrd_transaction_v2(txMsg)

