import sys
import os
import matplotlib.pyplot as plt
import pandas as pd
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QWidget,
    QListWidget,
    QPushButton,
    QFileDialog,
    QSizePolicy,
    QHBoxLayout
)

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar,
)

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class WaveformViewer(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Waveform Viewer')
        self.setGeometry(100, 100, 1600, 800)

        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        layout = QVBoxLayout()
        central_widget.setLayout(layout)

        # Create a horizontal layout for the listbox and the plot
        hbox = QWidget()
        hbox_layout = QHBoxLayout()
        hbox.setLayout(hbox_layout)

        # Listbox for displaying waveform files (on the left)
        self.file_listbox = QListWidget()
        self.file_listbox.itemClicked.connect(self.load_waveform)
        hbox_layout.addWidget(self.file_listbox)

        # Matplotlib plot (on the right)
        self.figure = Figure(figsize=(5, 4), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        hbox_layout.addWidget(self.canvas)

        # Navigation toolbar for zoom and pan
        self.nav_toolbar = NavigationToolbar(self.canvas, self)
        layout.addWidget(self.nav_toolbar)

        # Add the horizontal layout to the main layout
        layout.addWidget(hbox)

        # "Browse" button
        browse_button = QPushButton('Browse')
        browse_button.clicked.connect(self.select_data_directory)
        layout.addWidget(browse_button)

        # Set the initial directory
        self.data_directory = 'Results/continuous_logging_single'

        # Populate the listbox with files from the directory
        self.populate_file_listbox()

        self.canvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.file_listbox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)

    def populate_file_listbox(self):
        # Clear the listbox
        self.file_listbox.clear()

        # Get a list of waveform files in the directory
        if os.path.exists(self.data_directory) and os.path.isdir(self.data_directory):
            waveform_files = [file for file in os.listdir(self.data_directory) if file.endswith('.tsv')]

            # Add files to the listbox
            self.file_listbox.addItems(waveform_files)

    def select_data_directory(self):
        # Open a file dialog to select the data directory
        directory = QFileDialog.getExistingDirectory(self, 'Select Data Directory', self.data_directory)
        if directory:
            self.data_directory = directory
            self.populate_file_listbox()

    def load_waveform(self, item):
        # Load and plot the selected waveform file using pandas
        file_name = item.text()
        file_path = os.path.join(self.data_directory, file_name)

        try:
            df = pd.read_csv(file_path, sep='\t', header=None)

            # Create subplots for each pair of columns
            num_plots = df.shape[1] // 2
            self.figure.clear()
            ax = self.figure.add_subplot(1, 1, 1)
            ax.grid(True)
            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Measurmeent [V]/[A]')
            ax.set_title(file_name)
            lines = []
            for i in range(num_plots):
                #ax = self.figure.add_subplot(num_plots, 1, i + 1)
                time = df[i * 2]
                voltage = df[i * 2 + 1]
                line_current, = ax.plot(time, voltage, label=f'Plot {i+1}')
                lines.append(line_current)

            # Add legend and make lines toggleable
            box = ax.get_position()
            leg = ax.legend(fancybox=True, shadow=True, bbox_to_anchor=(0.97, 1), loc='upper left')
            lined = {}  # Will map legend lines to original lines.
            for legline, origline in zip(leg.get_lines(), lines):
                legline.set_picker(True)  # Enable picking on the legend line.
                lined[legline] = origline
    

            def on_pick(event):
                # On the pick event, find the original line corresponding to the legend
                # proxy line, and toggle its visibility.
                legline = event.artist
                origline = lined[legline]
                visible = not origline.get_visible()
                origline.set_visible(visible)
                # Change the alpha on the line in the legend, so we can see what lines
                # have been toggled.
                legline.set_alpha(1.0 if visible else 0.2)
                self.figure.canvas.draw()

            # Connect the pick event to the toggle function
            self.figure.canvas.mpl_connect('pick_event', on_pick)
                # Refresh the canvas to display the new plots
            self.canvas.draw()

        except Exception as e:
            print(f"Error loading waveform: {e}")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = WaveformViewer()
    window.show()
    sys.exit(app.exec_())
